<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<c:url value="/subject/add" var="subjectAdd"></c:url>
<c:url value="/subject/all" var="subjectAll"></c:url>

<c:url value="/student/add" var="studentAdd"></c:url>
<c:url value="/student/all" var="studentAll"></c:url>

<c:url value="/professor/add" var="professorAdd"></c:url>
<c:url value="/professor/all" var="professorAll"></c:url>

<c:url value="/exam/add" var="examAdd"></c:url>
<c:url value="/exam/all" var="examAll"></c:url>

<c:url value="/examRegistration/add" var="registerExam"></c:url>
<c:url value="/examRegistration/all" var="examRegistrationAll"></c:url>

<c:url value="/authentication/logout" var="logout"></c:url>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top ">

		<div class="container">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
				aria-expanded="false" aria-label="Toggle navigation"
			>
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
				<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Subject
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="${subjectAdd}">Add subject</a>
          <a class="dropdown-item" href="${subjectAll}">All subjects</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Professor
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="${professorAdd}">Add Professor</a>
          <a class="dropdown-item" href="${professorAll}">All Professors</a>
        </div>
      </li>
        <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Student
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="${studentAdd}">Add student</a>
          <a class="dropdown-item" href="${studentAll}">All students</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Exam
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="${examAdd}">Add Exam</a>
          <a class="dropdown-item" href="${examAll}">All Exams</a>
        </div>
      </li>
       <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Exam Registration
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="${registerExam}">Register Exam</a>
          <a class="dropdown-item" href="${examRegistrationAll}">Registered Exams</a>
        </div>
      </li>
				</ul>
				<span class="navbar-brand mb-0">User: ${sessionScope.user.username}</span><a class="nav-link mr-sm-2" href="${logout}"> Logout</a>
			</div>
		</div>
	</nav>
	
