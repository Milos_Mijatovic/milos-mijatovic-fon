<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title>
		<tiles:getAsString name="title"></tiles:getAsString>
	</title>
	<style type="text/css">
	body {
	padding-top: 56px;
    display: flex;
    flex-direction: column;
    height: 100vh;
}
.fill{
flex: 1;
}
.error {
	color: red;
}


</style>
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body data-gr-c-s-loaded="true">
	<header id ="header">
		<tiles:insertAttribute name="header"/>
	</header>
	
	<section id="site-content">
		<tiles:insertAttribute name="body"/>
	</section>

	<footer id ="footer"  class="fixed-bottom">
		<tiles:insertAttribute name="footer"/>
	</footer>
	<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	
</body>
</html>