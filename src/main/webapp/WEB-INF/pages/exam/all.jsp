<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container fill">
<table  class="table table-striped">
<tbody>
	<tr>
		<th>Subject</th>
		<th>Date</th>
		<th>Professor</th>
	</tr>
	<c:forEach items="${exams}" var="exam">
    	<tr>
    	<td>${exam.subjectDto.name}</td>
    	<td>${exam.date}</td>
    	<td>${exam.professorDto.fullname}</td>
    	 </tr>
</c:forEach>
</tbody>


</table>
</div>


