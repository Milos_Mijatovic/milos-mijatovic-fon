
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title></title>
<style>
body {
  padding-top: 56px;
    display: flex;
    flex-direction: column;
    height: 100vh;
}
.fill{
flex: 1;

}
</style>
</head>

<body data-gr-c-s-loaded="true">
<div class="container fill">
    <div class="row">
      <div class="col-md-7 offset-3 " >
        <br>
        <div class="card mb-4">
          <div class="card-body">
            <h2 class="card-title">Add Exam</h2>

  <form:form class ="horizontal" action="${pageContext.request.contextPath}/exam/save"
    method="post" modelAttribute="examDto">
    <fieldset>
    
          <div class="form-group row">
            <form:label path="subjectDto" class="col-sm-3 col-form-label">Subject</form:label>
            <div class="col-sm-8">
            <form:select path="subjectDto" class="form-control">
                <form:options items="${subjects}" itemValue="id" itemLabel="name" />
              </form:select>
               <div> <form:errors path="subjectDto" cssClass="alert-danger" /></div>
                </div>
                </div>
        <div class="form-group row">
           <form:label  class="col-sm-3 col-form-label" path="date">Exam Date</form:label>
           <div class="col-sm-8">
            <form:input type="date" path="date" id="dateId" class="form-control"/>
           <div> <form:errors path="date" cssClass="alert-danger" /></div>
         </div>
       </div>
        <div class="form-group row">
            <form:label path="professorDto" class="col-sm-3 col-form-label">Professor</form:label>
            <div class="col-sm-8">
            <form:select path="professorDto" class="form-control">
                <form:options items="${professors}" itemValue="id" itemLabel="fullname" />
              </form:select>
                <div><form:errors path="professorDto" cssClass="alert-danger" /></div>
                </div>
                </div>

            <button id="save" class="btn btn-primary">Save</button>
    </fieldset>
  </form:form>
    </div>
  </div>
</div>
</div>
</div>
</body>
</html>