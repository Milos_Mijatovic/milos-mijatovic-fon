<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
       <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container fill">
<table class="table table-striped">
<tbody>
	<tr>
			<th>First name</th>
    	<th>Last name </th>
    	<th>Email</th>
    	<th>Address</th>
    	<th>City</th>
    	<th>Phone</th>
    	<th>Reelection date</th>
    	<th>Title</th>
	</tr>
	<tr>
    	<td>${professorDto.firstname}</td>
    	<td>${professorDto.lastname} </td>
    	<td>${professorDto.email} </td>
    	<td>${professorDto.adress}</td>
    	<td>${professorDto.cityDto.name}</td>
    	<td>${professorDto.phone}</td>
    	<td>${professorDto.reelectionDate}</td>
    	<td>${professorDto.titleDto.name}</td>
    	</tr>
    	</tbody>
    	</table>
    	<div class="text-center">All Registered Exams</div>
    	    	<table class="table table-striped">
<tbody>
	<tr>
		<th>Subject</th>
		<th>Date</th>
	</tr>
	<c:forEach items="${examRegistrations}" var="examreg">
    	<tr>
    	<td>${examreg.examDto.subjectDto.name}</td>
    	<td>${examreg.examDto.date}</td>
    	 </tr>
</c:forEach>
</tbody>
</table>
    	</div>
