<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<div class="container fill">
<table class="table table-striped">
<tbody>
	<tr>
		<th>First name</th>
    	<th>Last name </th>
    	<th>Email</th>
    	<th>Address</th>
    	<th>City</th>
    	<th>Phone</th>
    	<th>Reelection date</th>
    	<th>Title</th>
    	<th>Details</th>
    	<th>Edit</th>
    	<th>Remove</th>
	</tr>
	<c:forEach items="${professors}" var="professor">
		
		<c:url value="/professor/remove"  var="professorRemove">
		<c:param name="id" value="${professor.id}"></c:param>
		</c:url>
		<c:url value="/professor/edit" var="professorEdit">
		<c:param name="id" value="${professor.id}"></c:param>
		</c:url>
		<c:url value="/professor/details" var="professorDetails">
		<c:param name="id" value="${professor.id}"></c:param>
		</c:url>
    	<tr>
    	<td>${professor.firstname}</td>
    	<td>${professor.lastname} </td>
    	<td>${professor.email} </td>
    	<td>${professor.adress}</td>
    	<td>${professor.cityDto.name}</td>
    	<td>${professor.phone}</td>
    	<td>${professor.reelectionDate}</td>
    	<td>${professor.titleDto.name}</td>
    	<td><a href="${professorDetails}">Details</a> </td>
    	<td><a href="${professorEdit}">Edit</a> </td>
    	<td><a href="${professorRemove}" onclick="return confirm('Are you sure you wanna remove this professor?')">Remove</a> </td>
    	 </tr>
</c:forEach>
</tbody>
</table>
<c:if test="${not empty errorMessage }"><div class="alert alert-danger" role="alert"> ${errorMessage}</div></c:if>
				</div>

