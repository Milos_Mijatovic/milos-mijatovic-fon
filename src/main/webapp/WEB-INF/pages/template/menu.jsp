<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title></title>
</head>
<body>

<div>
	<c:url value="/subject/add" var="subjectAdd"></c:url>
	<a href="<c:out value="${subjectAdd}"/>">Add Subject</a>
</div>

<div>
	<c:url value="/subject/all" var="subjectAll"></c:url>
	<a href="<c:out value="${subjectAll}"/>">All subjects</a>
</div>

<div>
	<c:url value="/subject/" var="subjectHome"></c:url>
	<a href="<c:out value="${subjectHome}"/>">Subject home</a>
</div>
<div>
	<c:url value="/student/add" var="studentAdd"></c:url>
	<a href="<c:out value="${studentAdd}"/>">Add Student</a>
</div>

<div>
	<c:url value="/student/all" var="studentAll"></c:url>
	<a href="<c:out value="${studentAll}"/>">All students</a>
</div>

<div>
	<c:url value="/student/" var="studentHome"></c:url>
	<a href="<c:out value="${studentHome}"/>">Student home</a>
</div>
<div>
	<c:url value="/professor/add" var="professorAdd"></c:url>
	<a href="<c:out value="${professorAdd}"/>">Add Professor</a>
</div>

<div>
	<c:url value="/professor/all" var="professorAll"></c:url>
	<a href="<c:out value="${professorAll}"/>">All Professors</a>
</div>

<div>
	<c:url value="/professor/" var="professorHome"></c:url>
	<a href="<c:out value="${professorHome}"/>">Professor home</a>
</div>

</body>
</html>