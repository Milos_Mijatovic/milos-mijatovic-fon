<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title></title>

<style>
body {
  padding-top: 56px;
    display: flex;
    flex-direction: column;
    height: 100vh;
}
.fill{
flex: 1;
}
</style>
</head>

<body data-gr-c-s-loaded="true">
 
<div class="container fill">
    <div class="row">
      <div class="col-md-7 offset-3">
        <br>
        <div class="card mb-4">
          <div class="card-body">
            <h2 class="card-title">Register Exam</h2>

  <form:form class ="horizontal" action="${pageContext.request.contextPath}/examRegistration/save"
    method="post" modelAttribute="examRegistrationDto">
    <fieldset>
          <div class="form-group row">
            <form:label path="examDto" class="col-sm-3 col-form-label">Exam</form:label>
            <div class="col-sm-8">
            <form:select path="examDto" class="form-control">
                <form:options items="${exams}" itemValue="id" itemLabel="subjectDto.name" />
              </form:select>
               <div> <form:errors path="examDto" cssClass="alert-danger" /></div>
                </div>
                </div>
        <div class="form-group row">
            <form:label path="studentDto" class="col-sm-3 col-form-label">Student</form:label>
            <div class="col-sm-8">
            <form:select path="studentDto" class="form-control">
                <form:options items="${students}" itemValue="id" itemLabel="fullname" />
              </form:select>
               <div> <form:errors path="studentDto" cssClass="alert-danger" /></div>
                </div>
                </div>

            <button id="save" class="btn btn-primary">Save</button>
    </fieldset>
  </form:form>
  <c:if test="${not empty errorMessage }"><div class="alert alert-danger" role="alert"> ${errorMessage}</div></c:if>
				</div>
    </div>
  </div>
</div>
</div>
</div>
</body>
</html>