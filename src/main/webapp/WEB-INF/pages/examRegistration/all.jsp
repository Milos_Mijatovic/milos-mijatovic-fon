<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container fill">
<table class="table table-striped">
<tbody>
	<tr>
		<th>Subject</th>
		<th>Professor</th>
		<th>Student</th>
	</tr>
	<c:forEach items="${examregs}" var="examreg">
    	<tr>
    	<td>${examreg.examDto.subjectDto.name}</td>
    	<td>${examreg.examDto.professorDto.fullname}</td>
    	<td>${examreg.studentDto.fullname}</td>
    	 </tr>
</c:forEach>
</tbody>
</table>
</div>
