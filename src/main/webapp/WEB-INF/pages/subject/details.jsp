<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<div class="container fill">
<table class="table table-striped">
<tbody>
	<tr>
		<th>Name</th>
		<th>Description</th>
    	<th>Semester </th>
    	<th>Year of study</th>
	</tr>
	<tr>
    	<td>${subjectDto.name}</td>
    	<td>${subjectDto.description}</td>
    	<td>${subjectDto.yearOfStudy} </td>
    	<td>${subjectDto.semester} </td>
    	</tr>
    	</tbody>
    	</table>
</div>