<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title></title>

<style>
body {
  padding-top: 56px;
    display: flex;
    flex-direction: column;
    height: 100vh;
}
.fill{
flex: 1;
}
</style>
</head>

<body data-gr-c-s-loaded="true">

<div class="container">
    <div class="row">
      <div class="col-md-7 offset-3">
        <br>
        <div class="card mb-4">
          <div class="card-body">
            <h2 class="card-title">Add Subject</h2>

  <form:form class ="form-horizontal" action="${pageContext.request.contextPath}/subject/save"
    method="post" modelAttribute="subjectDto"> 
    <fieldset>
      <div class="form-group row">
            <form:label path="name" class="col-sm-3 col-form-label">Name</form:label>
            <div class="col-sm-8">
            <form:input type="text" class="form-control" path="name"
                id="nameId" />
            <div><form:errors path="name"  cssClass="alert-danger" /></div>
            </div>
          </div>
             <div class="form-group row">
    <form:label for="description" class="col-sm-3 col-form-label" path="description">Description</form:label>
    <div class="col-sm-8">
    <form:textarea class="form-control" id="description" rows="3" path="description"></form:textarea>
    <div><form:errors path="description"  cssClass="alert-danger" /></div>
  </div>
  </div>

          <div class="form-group row">
            <form:label class="col-sm-3 col-form-label" path="yearOfStudy">Year of study</form:label>
            <div class="col-sm-8">
            <form:input type="text" path="yearOfStudy" class="form-control"
                id="yearOfStudyId" />
           <div><form:errors path="yearOfStudy" cssClass="alert-danger" /></div>
         </div>
         </div>
          <div class="form-group row">
            <form:label path="semester" class="col-sm-3 col-form-label">Semester</form:label>
            <div class="col-sm-8">
            <form:select path="semester" class="form-control">
                <form:option value="" label="Choose Semester"></form:option>
                <form:options items="${semesters}" itemValue="name" itemLabel="name" />
              </form:select>
                <div><form:errors path="semester" cssClass="alert-danger" /></div>
                </div>
                </div>
            <button id="save" class="btn btn-primary">Save</button>
            </div>
    </fieldset>
  </form:form>
    </div>
</div>
</div>
</div>
</body>
</html>
