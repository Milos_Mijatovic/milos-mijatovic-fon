<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container fill">
<table class="table table-striped">
<tbody>

	<tr>
		<th>Name</th>
		<th>Description</th>
    	<th>Semester </th>
    	<th>Year of study</th>
    	<th>Details</th>
    	<th>Edit</th>
    	<th>Remove</th>
	</tr>
	<c:forEach items="${subjects}" var="subject">
		
		<c:url value="/subject/remove"  var="subjectRemove">
		<c:param name="id" value="${subject.id}"></c:param>
		</c:url>
		<c:url value="/subject/edit" var="subjectEdit">
		<c:param name="id" value="${subject.id}"></c:param>
		</c:url>
		<c:url value="/subject/details" var="subjectDetails">
		<c:param name="id" value="${subject.id}"></c:param>
		</c:url>
    	<tr>
    	<td>${subject.name}</td>
    	<td>${subject.description}</td>
    	<td>${subject.semester} </td>
    	<td>${subject.yearOfStudy} </td>
    	
    	<td><a href="${subjectDetails}">Details</a> </td>
    	<td><a href="${subjectEdit}">Edit</a> </td>
    	<td><a href="${subjectRemove}" onclick="return confirm('Are you sure you wanna remove this subject?')">Remove</a> </td>
    	 </tr>
</c:forEach>
</tbody>
</table>
<c:if test="${not empty errorMessage }"><div class="alert alert-danger" role="alert"> ${errorMessage}</div></c:if>
				</div>
