<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="container fill">

<table class="table table-striped">

<tbody>
	<tr>
		<th>Index number</th>
		<th>First name</th>
    	<th>Last name </th>
    	<th>Email</th>
    	<th>Address</th>
    	<th>City</th>
    	<th>Phone</th>
    	<th>Current year</th>
	</tr>
	<tr>
    	<td>${studentDto.indexNumber}</td>
    	<td>${studentDto.firstname}</td>
    	<td>${studentDto.lastname} </td>
    	<td>${studentDto.email} </td>
    	<td>${studentDto.adress}</td>
    	<td>${studentDto.cityDto.name}</td>
    	<td>${studentDto.phone}</td>
    	<td>${studentDto.currentYearOfStudy}</td>
    	</tr>
    	</tbody>
    	</table>
    		<div class="text-center">All Registered Exams</div>
    	<table class="table table-striped">
<tbody>
	<tr>
		<th>Subject</th>
		<th>Professor</th>
		<th>Date</th>
	</tr>
	<c:forEach items="${examRegistrations}" var="examreg">
    	<tr>
    	<td>${examreg.examDto.subjectDto.name}</td>
    	<td>${examreg.examDto.professorDto.fullname}</td>
    	<td>${examreg.examDto.date}</td>
    	 </tr>
</c:forEach>
</tbody>
</table>
    	</div>
    	
    	
