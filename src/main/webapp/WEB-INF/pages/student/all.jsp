<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="container fill">
<table class="table table-striped">
<tbody>
	<tr>
		<th>Index number</th>
		<th>First name</th>
    	<th>Last name </th>
    	<th>Email</th>
    	<th>Address</th>
    	<th>City</th>
    	<th>Phone</th>
    	<th>Current year</th>
    	<th>Details</th>
    	<th>Edit</th>
    	<th>Remove</th>
	</tr>
	<c:forEach items="${students}" var="student">
		
		<c:url value="/student/remove"  var="studentRemove">
		<c:param name="id" value="${student.id}"></c:param>
		</c:url>
		<c:url value="/student/edit" var="studentEdit">
		<c:param name="id" value="${student.id}"></c:param>
		</c:url>
		<c:url value="/student/details" var="studentDetails">
		<c:param name="id" value="${student.id}"></c:param>
		</c:url>
    	<tr>
    	<td>${student.indexNumber}</td>
    	<td>${student.firstname}</td>
    	<td>${student.lastname} </td>
    	<td>${student.email} </td>
    	<td>${student.adress}</td>
    	<td>${student.cityDto.name}</td>
    	<td>${student.phone}</td>
    	<td>${student.currentYearOfStudy}</td>
    	<td><a href="${studentDetails}">Details</a> </td>
    	<td><a href="${studentEdit}">Edit</a> </td>
    	<td><a href="${studentRemove}" onclick="return confirm('Are you sure you wanna remove this subject?')">Remove</a> </td>
    	 </tr>
</c:forEach>
</tbody>
</table>
<c:if test="${not empty errorMessage }"><div class="alert alert-danger" role="alert"> ${errorMessage}</div></c:if>
				</div>
</div>