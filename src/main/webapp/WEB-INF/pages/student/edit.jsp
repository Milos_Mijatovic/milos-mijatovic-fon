<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title></title>
<style>
body {
  padding-top: 56px;
    display: flex;
    position: relative;
    flex-direction: column;
    height: 100vh;
}
.fill{
flex: 1;
}
</style>
</head>

<body data-gr-c-s-loaded="true">

<div class="container fill">
    <div class="row">
      <div class="col-md-7 offset-3">
        <br>
        <div class="card mb-4">
          <div class="card-body">
            <h2 class="card-title">Edit Student</h2>

  <form:form class ="horizontal" action="${pageContext.request.contextPath}/student/update"
    method="post" modelAttribute="studentDto">
    <fieldset>
	 <form:hidden path="id"
								id="idId" />
    <div class="form-group row">
            <form:label path="indexNumber" class="col-sm-3 col-form-label">Index Number</form:label>
            <div class="col-sm-8">
            <form:input type="text" class="form-control" path="indexNumber"
                id="indexNumberId" />
            <div><form:errors path="indexNumber"  cssClass="alert-danger" /></div>
            </div>
        </div>
      <div class="form-group row">
            <form:label path="firstname" class="col-sm-3 col-form-label">First name</form:label>
            <div class="col-sm-8">
            <form:input type="text" class="form-control" path="firstname"
                id="firstnameId" />
            <div><form:errors path="firstname"  cssClass="alert-danger" /></div>
            </div>
        </div>
          <div class="form-group row">
            <form:label class="col-sm-3 col-form-label" path="lastname">Last name</form:label>
            <div class="col-sm-8">
            <form:input type="text" path="lastname" class="form-control"
                id="lastnameId" />
           <div><form:errors path="lastname" cssClass="alert-danger" /></div>
         </div>
         </div>
           <div class="form-group row">
           <form:label  class="col-sm-3 col-form-label" path="email">Email</form:label>
           <div class="col-sm-8">
            <form:input type="text" path="email" id="emailId" class="form-control"/>
           <div> <form:errors path="email" cssClass="alert-danger" /></div>
         </div>
       </div>
             <div class="form-group row">
           <form:label  class="col-sm-3 col-form-label" path="adress">Address</form:label>
           <div class="col-sm-8">
            <form:input type="text" path="adress" id="adressId" class="form-control"/>
           <div> <form:errors path="adress" cssClass="alert-danger" /></div>
         </div>
       </div>
          <div class="form-group row">
            <form:label path="cityDto" class="col-sm-3 col-form-label">City</form:label>
            <div class="col-sm-8">
            <form:select path="cityDto" class="form-control">
                <form:option value="" label="Choose City"></form:option>
                <form:options items="${cities}" itemValue="id" itemLabel="name" />
              </form:select>
               <div> <form:errors path="cityDto" cssClass="alert-danger" /></div>
                </div>
                </div>
            <div class="form-group row">
           <form:label  class="col-sm-3 col-form-label" path="phone">Phone</form:label>
           <div class="col-sm-8">
            <form:input type="text" path="phone" id="phoneId" class="form-control"/>
           <div> <form:errors path="phone" cssClass="alert-danger" /></div>
         </div>
       </div>
        <div class="form-group row">
           <form:label  class="col-sm-3 col-form-label" path="currentYearOfStudy">Current Year</form:label>
           <div class="col-sm-8">
            <form:input type="text" path="currentYearOfStudy" id="currentYearOfStudyId" class="form-control"/>
           <div> <form:errors path="currentYearOfStudy" cssClass="alert-danger" /></div>
         </div>
       </div>

            <button id="save" class="btn btn-primary">Save</button>
    </fieldset>
  </form:form>
    </div>
  </div>
</div>
</div>
</div>
</body>
</html>
