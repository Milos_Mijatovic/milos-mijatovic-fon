<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login page</title>
<link rel='stylesheet'
	href='${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/css/bootstrap.min.css'>
</head>
<body>

	<form:form action="${pageContext.request.contextPath}/authentication/login" method="post" modelAttribute="userDto">
	<div class="container">
		<div class="row">
			<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
					<div class="card-body">
						<h5 class="card-title text-center">Sign In</h5>
						<form class="form-signin">
							<div class="form-label-group">
								<form:input type="username" path="username"  id="username" class="form-control"  placeholder="Username"></form:input>
								<label for="username">Username</label>
							</div>
							

							<div class="form-label-group">
								<form:input type="password"  id="password" class="form-control"  path="password" placeholder="Password"></form:input>
								<label for="password">Password</label>
							</div>
							<form:errors path="password" />


							<button class="btn btn-lg btn-primary btn-block text-uppercase"
								type="submit">Login</button>
						</form>
						
 

							
	
	
					</div>
					<c:if test="${not empty errorMessage }"><div class="alert alert-danger" role="alert"> ${errorMessage}</div></c:if>
				</div>
			</div>
		</div>
	</div>
	</form:form>

</body>


<script type="text/javascript" src="${pageContext.request.contextPath}/webjars/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/webjars/bootstrap/4.4.1/js/bootstrap.min.js"></script>


</html>