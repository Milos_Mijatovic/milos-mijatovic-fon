package milos.mijatovic.fon.config;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import milos.mijatovic.fon.formatter.CityDtoFormatter;
import milos.mijatovic.fon.formatter.ExamDtoFormatter;
import milos.mijatovic.fon.formatter.ProfessorDtoFormatter;
import milos.mijatovic.fon.formatter.ReelectionDateFormatter;
import milos.mijatovic.fon.formatter.StudentDtoFormatter;
import milos.mijatovic.fon.formatter.SubjectDtoFormatter;
import milos.mijatovic.fon.formatter.TitleDtoFormatter;
import milos.mijatovic.fon.interceptor.LoginInterceptor;
import milos.mijatovic.fon.service.CityService;
import milos.mijatovic.fon.service.ExamService;
import milos.mijatovic.fon.service.ProfessorService;
import milos.mijatovic.fon.service.StudentService;
import milos.mijatovic.fon.service.SubjectService;
import milos.mijatovic.fon.service.TitleService;



@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {
		"milos.mijatovic.fon.controller",
		"milos.mijatovic.fon.formatter",
		"milos.mijatovic.fon.interceptors",
		"milos.mijatovic.fon.exception"
		
})
public class MyWebContextConfig implements WebMvcConfigurer{
	private TitleService titleService;
	private CityService cityService;
	private ProfessorService professorService;
	private SubjectService subjectService;
	private ExamService examService;
	private StudentService studentService;
	
	@Autowired
	public MyWebContextConfig(CityService cityService, TitleService titleService, ProfessorService professorService, SubjectService subjectService, ExamService examService, StudentService studentService) {
		this.cityService = cityService;
		this.titleService = titleService;
		this.professorService = professorService;
		this.subjectService = subjectService;
		this.examService = examService;
		this.studentService = studentService;
	}
	@Bean
	public ViewResolver viewResolver() {
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/pages/");
		viewResolver.setSuffix(".jsp");
		viewResolver.setOrder(1);
		return viewResolver;
	}
	
	@Bean
	public ViewResolver tilesViewResolver() {
		TilesViewResolver tilesViewResolver = new TilesViewResolver();
		tilesViewResolver.setOrder(0);
		return tilesViewResolver;
	}
	

	@Bean
	public TilesConfigurer tilesCongigurer() {
		TilesConfigurer tilesConfigurer = new TilesConfigurer();
		tilesConfigurer.setDefinitions(
				new String[] {"/WEB-INF/views/tiles/tiles.xml"}
		);
		return tilesConfigurer;
	}
	
	@Bean(name = "simpleDateFormat")
	public SimpleDateFormat simpleDateFormat() {
		return new SimpleDateFormat("yyyy-MM-dd");
	}
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("redirect:/authentication/login");
	}
	@Override
	public void addFormatters(FormatterRegistry registry) {
		registry.addFormatter(new CityDtoFormatter(cityService));
		registry.addFormatter(new TitleDtoFormatter(titleService));
		registry.addFormatter(new ReelectionDateFormatter(simpleDateFormat()));
		registry.addFormatter(new ProfessorDtoFormatter(professorService));
		registry.addFormatter(new SubjectDtoFormatter(subjectService));
		registry.addFormatter(new ExamDtoFormatter(examService));
		registry.addFormatter(new StudentDtoFormatter(studentService));
	}
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
		  registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LoginInterceptor());
	}

}
