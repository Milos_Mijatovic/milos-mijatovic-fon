package milos.mijatovic.fon.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class MyWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	public MyWebApplicationInitializer() {
		System.out.println("Initializer");
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] {MyDatabaseConfig.class};
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] { MyWebContextConfig.class };
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

}
