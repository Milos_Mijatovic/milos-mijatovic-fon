package milos.mijatovic.fon.service;

import java.util.List;

import milos.mijatovic.fon.dto.ExamRegistrationDto;
import milos.mijatovic.fon.dto.StudentDto;
import milos.mijatovic.fon.exception.DataNotFoundException;

public interface ExamRegistrationService {
	public ExamRegistrationDto findById(Long id);
	public void save(ExamRegistrationDto examRegistrationDto);
	public List<ExamRegistrationDto> getAll();
	public List<ExamRegistrationDto> findAfterDate(ExamRegistrationDto examRegistrationDto);
	public List<ExamRegistrationDto> findByStudent(StudentDto studentDto) throws DataNotFoundException;
	public void findAllExamsAfterDateByName(ExamRegistrationDto examRegistrationDto) throws DataNotFoundException;
	public List<ExamRegistrationDto> currentlyRegisteredExams(Long id);
	public List<ExamRegistrationDto> registeredExamsProfessor(Long id);
}
