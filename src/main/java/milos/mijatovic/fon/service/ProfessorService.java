package milos.mijatovic.fon.service;

import java.util.List;

import milos.mijatovic.fon.dto.ProfessorDto;


public interface ProfessorService {
	public ProfessorDto findById(Long id);
	public void save(ProfessorDto professorDto);
	public List<ProfessorDto> getAll();
	public void deleteById(Long id);
	public List<ProfessorDto> findByEmail(ProfessorDto professorDto);
	public List<ProfessorDto> findByEmail(String email);
}
