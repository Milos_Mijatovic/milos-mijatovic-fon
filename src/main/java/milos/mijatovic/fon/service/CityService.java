package milos.mijatovic.fon.service;

import java.util.List;

import milos.mijatovic.fon.dto.CityDto;

public interface CityService {
	public CityDto findById(Long id);
	public void save(CityDto cityDto);
	public List<CityDto> getAll();
	public void deleteById(Long id);
}
