package milos.mijatovic.fon.service.impl;


import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import milos.mijatovic.fon.converter.SubjectConverter;
import milos.mijatovic.fon.dto.SubjectDto;
import milos.mijatovic.fon.entity.SubjectEntity;
import milos.mijatovic.fon.repository.SubjectRepository;
import milos.mijatovic.fon.service.SubjectService;
@Service
@Transactional
public class SubjectServiceImpl implements SubjectService{
	
	
	private SubjectRepository subjectRepository;
	private SubjectConverter subjectConverter;
	
	
	@Autowired
	public SubjectServiceImpl(SubjectRepository subjectRepository, SubjectConverter subjectConverter) {
		this.subjectRepository = subjectRepository;
		this.subjectConverter = subjectConverter;
	}
	

	@Override
	public SubjectDto findById(Long id) {
		Optional<SubjectEntity> subjectOptional = subjectRepository.findById(id);
		if(subjectOptional.isPresent()) {
			return subjectConverter.entityToDto(subjectOptional.get());
		} else {
			return null; 
		}
	}


	@Override
	public void save(SubjectDto subjectDto) {
		subjectRepository.save(subjectConverter.dtoToEntity(subjectDto));
		
	}



	@Override
	public List<SubjectDto> getAll() {
		List<SubjectEntity> list = subjectRepository.findAll();
		List<SubjectDto> subjectDtos = subjectConverter.entityToDtoList(list);
		return subjectDtos;
	}



	@Override
	public void deleteById(Long id) {
		subjectRepository.deleteById(id);
		
	}



	
	
}
