package milos.mijatovic.fon.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import milos.mijatovic.fon.converter.ExamConverter;
import milos.mijatovic.fon.converter.ProfessorConverter;
import milos.mijatovic.fon.converter.SubjectConverter;
import milos.mijatovic.fon.dto.ExamDto;
import milos.mijatovic.fon.dto.ProfessorDto;
import milos.mijatovic.fon.dto.SubjectDto;
import milos.mijatovic.fon.entity.ExamEntity;
import milos.mijatovic.fon.exception.DataNotFoundException;
import milos.mijatovic.fon.repository.ExamRepository;
import milos.mijatovic.fon.service.ExamService;

@Service
@Transactional
public class ExamServiceImpl implements ExamService{

	private final ExamRepository examRepository;
	private final ExamConverter examConverter;
	private final SimpleDateFormat simpleDateFormat;
	private final ProfessorConverter professorConverter;
	private final SubjectConverter subjectConverter;

	
	@Autowired
	public ExamServiceImpl(ExamRepository examRepository, ExamConverter examConverter, @Qualifier("simpleDateFormat")SimpleDateFormat simpleDateFormat, ProfessorConverter professorConverter, SubjectConverter subjectConverter) {
		this.examRepository = examRepository;
		this.examConverter = examConverter;
		this.simpleDateFormat = simpleDateFormat;
		this.professorConverter = professorConverter;
		this.subjectConverter = subjectConverter;
	}
	
	
	@Override
	public ExamDto findById(Long id) {
		Optional<ExamEntity> cityOptional = examRepository.findById(id);
		if (cityOptional.isPresent()) {
			return examConverter.entityToDto(cityOptional.get());
		} else {
			return null;
		}
		
	}

	@Override
	public void save(ExamDto examDto) {
		examRepository.save(examConverter.dtoToEntity(examDto));
	}

	@Override
	public List<ExamDto> getAll() {
		List<ExamEntity> list = examRepository.findAll();
		List<ExamDto> examDtos = examConverter.entityToDtoList(list);
		return examDtos;
	}


	@Override
	public List<ExamDto> findAfterDate(ExamDto examDto) {
		Date currentDate = new Date();
		Date formattedDate;
		try {
			formattedDate = simpleDateFormat.parse((simpleDateFormat.format((currentDate))));
			Date yesterday = new Date(formattedDate.getTime() - (1000 * 60 * 60 * 24));
			List<ExamEntity> list = examRepository.findByDateAfter(yesterday, examDto.getSubjectDto().getName());
			List<ExamDto>  examDtos = examConverter.entityToDtoList(list);
			return examDtos;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new ArrayList<ExamDto>();
		
	}
	@Override
	public List<ExamDto> findExamsWeekBefore() {
		Date currentDate = new Date();
		Date formattedDate;
		try {
			formattedDate = simpleDateFormat.parse((simpleDateFormat.format((currentDate))));
			Date weekAfter = new Date(formattedDate.getTime() + 7*(1000 * 60 * 60 * 24));
			List<ExamEntity> list = examRepository.findByDateBetween(formattedDate, weekAfter);
			List<ExamDto>  examDtos = examConverter.entityToDtoList(list);
			return examDtos;
		

		
	}catch (ParseException e) {
		e.printStackTrace();
	}
		return new ArrayList<ExamDto>();

}


	@Override
	public List<ExamDto> findByProfessor(ProfessorDto professorDto) throws DataNotFoundException {
		List<ExamEntity> examEntities = examRepository.findByProfessor(professorConverter.dtoToEntity(professorDto));
		List<ExamDto> examDto = examConverter.entityToDtoList(examEntities);
		if(!examEntities.isEmpty()) {
			throw new DataNotFoundException("Cannot remove professor, he has an active exam!");
		}
		return examDto;
	}


	@Override
	public List<ExamDto> findBySubject(SubjectDto subjectDto) throws DataNotFoundException {
		List<ExamEntity> examEntities = examRepository.findBySubject(subjectConverter.dtoToEntity(subjectDto));
		List<ExamDto> examDto = examConverter.entityToDtoList(examEntities);
		if(!examEntities.isEmpty()) {
			throw new DataNotFoundException("Cannot remove subject, it has an active exam!");
		}
		return examDto;
		
	}
}
