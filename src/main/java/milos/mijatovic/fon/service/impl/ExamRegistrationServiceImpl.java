package milos.mijatovic.fon.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import milos.mijatovic.fon.converter.ExamRegistrationConverter;
import milos.mijatovic.fon.converter.StudentConverter;
import milos.mijatovic.fon.dto.ExamRegistrationDto;
import milos.mijatovic.fon.dto.StudentDto;
import milos.mijatovic.fon.entity.ExamRegistrationEntity;
import milos.mijatovic.fon.exception.DataNotFoundException;
import milos.mijatovic.fon.repository.ExamRegistrationRepository;
import milos.mijatovic.fon.service.ExamRegistrationService;

@Service
@Transactional
public class ExamRegistrationServiceImpl implements ExamRegistrationService {

	private final ExamRegistrationRepository examRegistrationRepository;
	private final ExamRegistrationConverter examRegistrationConverter;
	private final StudentConverter studentConverter;
	private final SimpleDateFormat simpleDateFormat;

	
	@Autowired
	public ExamRegistrationServiceImpl(ExamRegistrationRepository examRegistrationRepository, ExamRegistrationConverter examRegistrationConverter, @Qualifier("simpleDateFormat")SimpleDateFormat simpleDateFormat, StudentConverter studentConverter) {
		this.examRegistrationRepository = examRegistrationRepository;
		this.examRegistrationConverter = examRegistrationConverter;
		this.simpleDateFormat = simpleDateFormat;
		this.studentConverter = studentConverter;
	}
	
	@Override
	public ExamRegistrationDto findById(Long id) {
		Optional<ExamRegistrationEntity> examRegOptional = examRegistrationRepository.findById(id);
		if (examRegOptional.isPresent()) {
			return examRegistrationConverter.entityToDto(examRegOptional.get());
		} else {
			return null;
		}
	}

	@Override
	public void save(ExamRegistrationDto examRegistrationDto) {
		examRegistrationRepository.save(examRegistrationConverter.dtoToEntity(examRegistrationDto));
	}

	@Override
	public List<ExamRegistrationDto> getAll() {
		List<ExamRegistrationEntity> list = examRegistrationRepository.findAll();
		List<ExamRegistrationDto> examDtos = examRegistrationConverter.entityToDtoList(list);
		return examDtos;
	}

	@Override
	public List<ExamRegistrationDto> findAfterDate(ExamRegistrationDto examRegistrationDto) {
		return null;
	}

	@Override
	public List<ExamRegistrationDto> findByStudent(StudentDto studentDto) throws DataNotFoundException {
		List<ExamRegistrationEntity> examRegEntities = examRegistrationRepository.findByStudentEntity(studentConverter.dtoToEntity(studentDto));
		List<ExamRegistrationDto> examDto = examRegistrationConverter.entityToDtoList(examRegEntities);
		if(!examRegEntities.isEmpty()) {
			throw new DataNotFoundException("Cannot remove student, he has a registered exam!");
		}
		return examDto;
	}

	@Override
	public void findAllExamsAfterDateByName(ExamRegistrationDto examRegistrationDto) throws DataNotFoundException {
		
		Date currentDate = new Date();
		Date formattedDate;
		try {
			formattedDate = simpleDateFormat.parse((simpleDateFormat.format((currentDate))));
			Date yesterday = new Date(formattedDate.getTime() - (1000 * 60 * 60 * 24));
			List<ExamRegistrationEntity> list = examRegistrationRepository.findAllExamsAfterDateByName(yesterday, examRegistrationDto.getExamDto().getSubjectDto().getId(), examRegistrationDto.getStudentDto().getId());
			if(!list.isEmpty()) {
				throw new DataNotFoundException("Student has already registered this exam!", examRegistrationDto);
			}
		

		
	}catch (ParseException e) {
		e.printStackTrace();
	}
	}

	@Override
	public List<ExamRegistrationDto> currentlyRegisteredExams(Long id) {
			List<ExamRegistrationEntity> list = examRegistrationRepository.currentlyRegisteredExams(id);
			List<ExamRegistrationDto> examDto = examRegistrationConverter.entityToDtoList(list);
			return examDto;
	}
	@Override
	public List<ExamRegistrationDto> registeredExamsProfessor(Long id){
		List<ExamRegistrationEntity> list = examRegistrationRepository.currentlyRegisteredExamsProfessor(id);
		List<ExamRegistrationDto> examDto = examRegistrationConverter.entityToDtoList(list);
		return examDto;
	}
	

}
