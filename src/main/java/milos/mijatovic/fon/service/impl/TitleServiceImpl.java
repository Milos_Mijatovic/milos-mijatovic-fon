package milos.mijatovic.fon.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import milos.mijatovic.fon.converter.TitleConverter;
import milos.mijatovic.fon.dto.TitleDto;
import milos.mijatovic.fon.entity.TitleEntity;
import milos.mijatovic.fon.repository.TitleRepository;
import milos.mijatovic.fon.service.TitleService;

@Service
@Transactional
public class TitleServiceImpl implements TitleService{

	
	private TitleRepository titleRepository;
	private TitleConverter titleConverter;

	@Autowired
	public TitleServiceImpl(TitleRepository titleRepository, TitleConverter titleConverter) {
		this.titleRepository = titleRepository;
		this.titleConverter = titleConverter;
	}
	
	@Override
	public TitleDto findById(Long id) {
		Optional<TitleEntity> titleOptional = titleRepository.findById(id);
		if (titleOptional.isPresent()) {
			return titleConverter.entityToDto(titleOptional.get());
		} else {
			return null;
		}
	}

	@Override
	public void save(TitleDto titleDto) {
		titleRepository.save(titleConverter.dtoToEntity(titleDto));
	}

	@Override
	public List<TitleDto> getAll() {
		List<TitleEntity> list = titleRepository.findAll();
		List<TitleDto> titleDtos = titleConverter.entityToDtoList(list);
		return titleDtos;
	}

	@Override
	public void deleteById(Long id) {
		titleRepository.deleteById(id);
	}

}
