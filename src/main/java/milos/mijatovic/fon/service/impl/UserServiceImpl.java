package milos.mijatovic.fon.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import milos.mijatovic.fon.converter.UserConverter;
import milos.mijatovic.fon.dto.UserDto;
import milos.mijatovic.fon.entity.UserEntity;
import milos.mijatovic.fon.exception.UserValidationException;
import milos.mijatovic.fon.repository.UserRepository;
import milos.mijatovic.fon.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	private final UserRepository userRepository;
	private final UserConverter userConverter;
	
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository, UserConverter userConverter) {
		this.userRepository = userRepository;
		this.userConverter = userConverter;
	}
	
	@Override
	public List<UserDto> findUserByNameAndPassword(UserDto userDto) throws UserValidationException {
		
		List<UserEntity> list = userRepository.findByUsernameAndPassword(userDto.getUsername(), userDto.getPassword());
		List<UserDto> userDtos = userConverter.entityToDtoList(list);
		if(userDtos.isEmpty()) {
			throw new UserValidationException("Wrong username/password");
		}
		return userDtos;
	}

}
