package milos.mijatovic.fon.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import milos.mijatovic.fon.converter.ProfessorConverter;
import milos.mijatovic.fon.dto.ProfessorDto;
import milos.mijatovic.fon.entity.ProfessorEntity;
import milos.mijatovic.fon.repository.ProfessorRepository;
import milos.mijatovic.fon.service.ProfessorService;

@Service
@Transactional
public class ProfessorServiceImpl implements ProfessorService{

	
	private ProfessorRepository professorRepository;
	private ProfessorConverter professorConverter;

	@Autowired
	public ProfessorServiceImpl(ProfessorRepository professorRepository, ProfessorConverter professorConverter) {
		this.professorRepository = professorRepository;
		this.professorConverter = professorConverter;
	}
	
	
	@Override
	public ProfessorDto findById(Long id) {
		Optional<ProfessorEntity> professOptional = professorRepository.findById(id);
		if(professOptional.isPresent()) {
			return professorConverter.entityToDto(professOptional.get());
		} else {
			return null; 
		}
	}

	@Override
	public void save(ProfessorDto professorDto) {
		professorRepository.save(professorConverter.dtoToEntity(professorDto));
	}

	@Override
	public List<ProfessorDto> getAll() {
		List<ProfessorEntity> list = professorRepository.findAll();
		List<ProfessorDto> professorDtos = professorConverter.entityToDtoList(list);
		return professorDtos;
	}

	@Override
	public void deleteById(Long id) {
		professorRepository.deleteById(id);
	}


	@Override
	public List<ProfessorDto> findByEmail(ProfessorDto professorDto) {
		
		return professorConverter.entityToDtoList(professorRepository.findByEmailIgnoreCase(professorDto.getEmail()));
	}


	@Override
	public List<ProfessorDto> findByEmail(String email) {
		return	professorConverter.entityToDtoList(professorRepository.findByEmailIgnoreCase(email));
	}

}
