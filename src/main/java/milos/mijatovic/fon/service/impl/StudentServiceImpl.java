package milos.mijatovic.fon.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import milos.mijatovic.fon.converter.StudentConverter;
import milos.mijatovic.fon.dto.StudentDto;
import milos.mijatovic.fon.entity.StudentEntity;
import milos.mijatovic.fon.repository.StudentRepository;
import milos.mijatovic.fon.service.StudentService;

@Service
@Transactional
public class StudentServiceImpl implements StudentService{

	
	
	private StudentRepository studentRepository;
	private StudentConverter studentConverter;

	@Autowired
	public StudentServiceImpl(StudentRepository studentRepository, StudentConverter studentConverter) {
		this.studentRepository = studentRepository;
		this.studentConverter = studentConverter;
	}

	
	@Override
	public StudentDto findById(Long id) {
		Optional<StudentEntity> studentOptional = studentRepository.findById(id);
		if(studentOptional.isPresent()) {
			return studentConverter.entityToDto(studentOptional.get());
		} else {
			return null; 
		}
	}

	@Override
	public void save(StudentDto studentDto) {
		studentRepository.save(studentConverter.dtoToEntity(studentDto));
	}

	@Override
	public List<StudentDto> getAll() {
		List<StudentEntity> list = studentRepository.findAll();
		List<StudentDto> studentDtos = studentConverter.entityToDtoList(list);
		return studentDtos;
	}

	@Override
	public void deleteById(Long id) {
		studentRepository.deleteById(id);
	}


	@Override
	public List<StudentDto> findByEmail(StudentDto studentDto) {
		return studentConverter.entityToDtoList(studentRepository.findByEmailIgnoreCase(studentDto.getEmail()));
	}


	@Override
	public List<StudentDto> findByIndexNumber(StudentDto studentDto) {
		return studentConverter.entityToDtoList(studentRepository.findByIndexNumber(studentDto.getIndexNumber()));
	}


	@Override
	public List<StudentDto> findByEmail(String email) {
		return studentConverter.entityToDtoList(studentRepository.findByEmailIgnoreCase(email));
	}

}
