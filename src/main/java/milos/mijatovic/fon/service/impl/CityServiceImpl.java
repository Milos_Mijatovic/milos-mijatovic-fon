package milos.mijatovic.fon.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import milos.mijatovic.fon.converter.CityConverter;
import milos.mijatovic.fon.dto.CityDto;
import milos.mijatovic.fon.entity.CityEntity;
import milos.mijatovic.fon.repository.CityRepository;

import milos.mijatovic.fon.service.CityService;

@Service
@Transactional
public class CityServiceImpl implements CityService {

	private CityRepository cityRepository;
	private CityConverter cityConverter;

	@Autowired
	public CityServiceImpl(CityRepository cityRepository, CityConverter cityConverter) {
		this.cityRepository = cityRepository;
		this.cityConverter = cityConverter;
	}

	@Override
	public CityDto findById(Long id) {
		Optional<CityEntity> cityOptional = cityRepository.findById(id);
		if (cityOptional.isPresent()) {
			return cityConverter.entityToDto(cityOptional.get());
		} else {
			return null;
		}
	}

	@Override
	public void save(CityDto cityDto) {
		cityRepository.save(cityConverter.dtoToEntity(cityDto));
	}

	@Override
	public List<CityDto> getAll() {
		List<CityEntity> list = cityRepository.findAll();
		List<CityDto> cityDtos = cityConverter.entityToDtoList(list);
		return cityDtos;
	}

	@Override
	public void deleteById(Long id) {
		cityRepository.deleteById(id);
	}

}
