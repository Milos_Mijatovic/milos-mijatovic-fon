package milos.mijatovic.fon.service;

import java.util.List;


import milos.mijatovic.fon.dto.TitleDto;

public interface TitleService {
	public TitleDto findById(Long id);
	public void save(TitleDto titleDto);
	public List<TitleDto> getAll();
	public void deleteById(Long id);
}
