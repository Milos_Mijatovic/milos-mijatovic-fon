package milos.mijatovic.fon.service;


import java.util.List;

import milos.mijatovic.fon.dto.SubjectDto;

public interface SubjectService {

	public SubjectDto findById(Long id);
	public void save(SubjectDto subjectDto);
	public List<SubjectDto> getAll();
	public void deleteById(Long id);
	
}
