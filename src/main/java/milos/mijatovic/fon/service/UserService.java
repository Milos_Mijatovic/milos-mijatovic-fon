package milos.mijatovic.fon.service;

import java.util.List;

import milos.mijatovic.fon.dto.UserDto;
import milos.mijatovic.fon.exception.UserValidationException;


public interface UserService {

	public List<UserDto> findUserByNameAndPassword(UserDto userDto) throws UserValidationException;
}
