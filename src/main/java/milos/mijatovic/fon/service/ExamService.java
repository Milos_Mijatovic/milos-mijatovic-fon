package milos.mijatovic.fon.service;

import java.util.List;

import milos.mijatovic.fon.dto.ExamDto;
import milos.mijatovic.fon.dto.ProfessorDto;
import milos.mijatovic.fon.dto.SubjectDto;
import milos.mijatovic.fon.exception.DataNotFoundException;

public interface ExamService {
	public ExamDto findById(Long id);
	public void save(ExamDto examDto);
	public List<ExamDto> getAll();
	public List<ExamDto> findAfterDate(ExamDto examDto);
	List<ExamDto> findExamsWeekBefore();
	public List<ExamDto> findByProfessor(ProfessorDto professorDto) throws DataNotFoundException;
	public List<ExamDto> findBySubject(SubjectDto subjectDto) throws DataNotFoundException;
}
	
