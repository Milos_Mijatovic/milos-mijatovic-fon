package milos.mijatovic.fon.service;

import java.util.List;

import milos.mijatovic.fon.dto.StudentDto;

public interface StudentService {
	public StudentDto findById(Long id);
	public void save(StudentDto studentDto);
	public List<StudentDto> getAll();
	public void deleteById(Long id);
	public List<StudentDto> findByEmail(StudentDto studentDto);
	public List<StudentDto> findByEmail(String email);
	public List<StudentDto> findByIndexNumber(StudentDto studentDto);
}
