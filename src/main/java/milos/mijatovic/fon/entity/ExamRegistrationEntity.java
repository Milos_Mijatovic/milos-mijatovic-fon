package milos.mijatovic.fon.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Exam_Registration")
public class ExamRegistrationEntity implements Serializable{
	

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "Exam_id",referencedColumnName = "id", nullable = false)
	private ExamEntity examEntity;
	@ManyToOne
	@JoinColumn(name = "Student_id",referencedColumnName = "id", nullable = false)
	private StudentEntity studentEntity;
	
	
	
	
	
	public ExamRegistrationEntity() {
	}
	
	
	
	public ExamRegistrationEntity(Long id, ExamEntity examEntity, StudentEntity studentEntity) {
		super();
		this.id = id;
		this.examEntity = examEntity;
		this.studentEntity = studentEntity;
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ExamEntity getExamEntity() {
		return examEntity;
	}
	public void setExamEntity(ExamEntity examEntity) {
		this.examEntity = examEntity;
	}
	public StudentEntity getStudentEntity() {
		return studentEntity;
	}
	public void setStudentEntity(StudentEntity studentEntity) {
		this.studentEntity = studentEntity;
	}



	@Override
	public String toString() {
		return "ExamRegistrationEntity [id=" + id + ", examEntity=" + examEntity + ", studentEntity=" + studentEntity
				+ "]";
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((examEntity == null) ? 0 : examEntity.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((studentEntity == null) ? 0 : studentEntity.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamRegistrationEntity other = (ExamRegistrationEntity) obj;
		if (examEntity == null) {
			if (other.examEntity != null)
				return false;
		} else if (!examEntity.equals(other.examEntity))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (studentEntity == null) {
			if (other.studentEntity != null)
				return false;
		} else if (!studentEntity.equals(other.studentEntity))
			return false;
		return true;
	}
	
	
	
	
	
}
