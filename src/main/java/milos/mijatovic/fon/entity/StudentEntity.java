package milos.mijatovic.fon.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "Student")
public class StudentEntity implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "IndexNumber", length = 10, nullable =  false, unique = true)
	private String indexNumber;
	@Column(name = "FirstName", length = 30, nullable =  false)
	private String firstname;
	@Column(name = "LastName", length = 30, nullable =  false)
	private String lastname;
	@Column(name = "Email", length = 30, unique = true)
	private String email;
	@Column(name="Adress", length = 50)
	private String adress;
	@ManyToOne
	@JoinColumn(name="City_id")
	private CityEntity cityEntity;
	@Column(name = "Phone", length = 15)
	private String phone;
	@Column(name = "CurrentYearOfStudy", nullable =  false, length = 7)
	private int currentYearOfStudy;
	public StudentEntity() {
		super();
	}
	public StudentEntity(Long id, String indexNumber, String firstname, String lastname, String email, String adress,
			CityEntity cityEntity, String phone, int currentYearOfStudy) {
		super();
		this.id = id;
		this.indexNumber = indexNumber;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.adress = adress;
		this.cityEntity = cityEntity;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIndexNumber() {
		return indexNumber;
	}
	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public CityEntity getCityEntity() {
		return cityEntity;
	}
	public void setCityEntity(CityEntity cityEntity) {
		this.cityEntity = cityEntity;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}
	public void setCurrentYearOfStudy(int currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}
	@Override
	public String toString() {
		return "StudentEntity [id=" + id + ", indexNumber=" + indexNumber + ", firstname=" + firstname + ", lastname="
				+ lastname + ", email=" + email + ", adress=" + adress + ", phone=" + phone + ", currentYearOfStudy="
				+ currentYearOfStudy + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentEntity other = (StudentEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
