package milos.mijatovic.fon.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "Exam")
public class ExamEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "subject_id",referencedColumnName = "id", nullable = false)
	private SubjectEntity subject;

	@ManyToOne
	@JoinColumn(name = "professor_id",referencedColumnName = "id", nullable = false)
	private ProfessorEntity professor;

	@Temporal(TemporalType.DATE)
	@Column(name="exam_date", nullable = false)
	private Date date;

	public ExamEntity(Long id, SubjectEntity subject, ProfessorEntity professor, Date date) {
		super();
		this.id = id;
		this.subject = subject;
		this.professor = professor;
		this.date = date;
	}

	public ExamEntity() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SubjectEntity getSubject() {
		return subject;
	}

	public void setSubject(SubjectEntity subject) {
		this.subject = subject;
	}

	public ProfessorEntity getProfessor() {
		return professor;
	}

	public void setProfesor(ProfessorEntity professor) {
		this.professor = professor;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExamEntity other = (ExamEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}




	
	
	
	
}
