package milos.mijatovic.fon.entity;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import milos.mijatovic.fon.enums.Semester;


@Entity
@Table(name = "Subject")
public class SubjectEntity implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "Name", length = 30, nullable = false)
	private String name;
	@Column(name = "Description", length = 200)
	private String description;
	@Column(name = "YearOfStudy")
	private int yearOfStudy;
	@Enumerated(EnumType.STRING)
	@Column(name = "Semester", length = 10)
	private Semester semester;


	
	
	
	public SubjectEntity(Long id, String name, String description, int yearOfStudy, Semester semester
			) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;

	}
	public SubjectEntity() {
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getYearOfStudy() {
		return yearOfStudy;
	}
	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}
	public Semester getSemester() {
		return semester;
	}
	public void setSemester(Semester semester) {
		this.semester = semester;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

	@Override
	public String toString() {
		return "SubjectEntity [id=" + id + ", name=" + name + ", yearOfStudy=" + yearOfStudy + ", semester=" + semester
				+ "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubjectEntity other = (SubjectEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
