package milos.mijatovic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import milos.mijatovic.fon.dto.StudentDto;
import milos.mijatovic.fon.service.StudentService;
@Component
public class StudentDtoFormatter implements Formatter<StudentDto>{

	
	private final StudentService studentService;
	
	public StudentDtoFormatter(StudentService studentService) {
		this.studentService = studentService;
	}
	
	
	@Override
	public String print(StudentDto object, Locale locale) {
		return object.toString();
	}

	@Override
	public StudentDto parse(String text, Locale locale) throws ParseException {
		
		Long id = Long.parseLong(text);
		StudentDto studentDto = studentService.findById(id);		
		return studentDto;
		
	}

}
