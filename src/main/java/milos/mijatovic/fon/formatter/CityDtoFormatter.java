package milos.mijatovic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import milos.mijatovic.fon.dto.CityDto;
import milos.mijatovic.fon.service.CityService;

@Component
public class CityDtoFormatter implements Formatter<CityDto>{

private final CityService cityService;
	
	@Autowired
	public CityDtoFormatter(CityService cityService) {
		this.cityService = cityService;
	}
	@Override
	public String print(CityDto cityDto, Locale locale) {
		return cityDto.toString();
	}

	@Override
	public CityDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		CityDto cityDto = cityService.findById(id);		
		return cityDto;
	}

}
