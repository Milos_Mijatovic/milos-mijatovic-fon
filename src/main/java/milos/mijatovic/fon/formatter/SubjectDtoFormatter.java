package milos.mijatovic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import milos.mijatovic.fon.dto.SubjectDto;
import milos.mijatovic.fon.service.SubjectService;
@Component
public class SubjectDtoFormatter implements Formatter<SubjectDto>{

	private final SubjectService subjectService;
	@Autowired
	public SubjectDtoFormatter(SubjectService subjectService) {
		this.subjectService = subjectService;
	}
	
	
	@Override
	public String print(SubjectDto object, Locale locale) {
		return object.toString();
	}

	@Override
	public SubjectDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		return subjectService.findById(id);
	}

}
