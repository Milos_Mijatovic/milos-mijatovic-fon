package milos.mijatovic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import milos.mijatovic.fon.dto.ProfessorDto;
import milos.mijatovic.fon.service.ProfessorService;
@Component
public class ProfessorDtoFormatter implements Formatter<ProfessorDto> {

	private final ProfessorService professorService;
	
	@Autowired
	public ProfessorDtoFormatter(ProfessorService professorService) {
		this.professorService = professorService;
	}
	
	@Override
	public String print(ProfessorDto object, Locale locale) {
		return object.toString();
	}

	@Override
	public ProfessorDto parse(String text, Locale locale) throws ParseException {
		
		Long id = Long.parseLong(text);
		ProfessorDto professorDto = professorService.findById(id);
		return professorDto;
		
	}

}
