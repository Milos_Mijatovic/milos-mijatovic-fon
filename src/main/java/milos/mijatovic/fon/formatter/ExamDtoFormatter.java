package milos.mijatovic.fon.formatter;

import java.text.ParseException;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import milos.mijatovic.fon.dto.ExamDto;
import milos.mijatovic.fon.service.ExamService;
@Component
public class ExamDtoFormatter implements Formatter<ExamDto> {

	private final ExamService examService;
	
	@Autowired
	public  ExamDtoFormatter(ExamService examService) {
		this.examService = examService;
	}
	
	
	@Override
	public String print(ExamDto object, Locale locale) {
		return object.toString();
	}

	@Override
	public ExamDto parse(String text, Locale locale) throws ParseException {
		Long id = Long.parseLong(text);
		ExamDto examDto = examService.findById(id);
		return examDto;
	}

}
