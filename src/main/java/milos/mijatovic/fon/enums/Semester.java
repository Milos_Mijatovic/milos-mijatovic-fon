package milos.mijatovic.fon.enums;

public enum Semester {
	Summer("Summer"), Winter("Winter");
	private String name;
	
	private Semester(String name) {
		this.name=name;
	}

	public String getName() {
		return name;
	}
}
