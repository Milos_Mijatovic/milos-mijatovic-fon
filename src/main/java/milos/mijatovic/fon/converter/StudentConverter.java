package milos.mijatovic.fon.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import milos.mijatovic.fon.dto.StudentDto;
import milos.mijatovic.fon.entity.StudentEntity;
@Component
public class StudentConverter {
	
	private final CityConverter cityConverter;
	
	@Autowired
	public StudentConverter(CityConverter cityConverter) {
		this.cityConverter = cityConverter;
	}
	
	
	
	public StudentDto entityToDto(StudentEntity studentEntity) {
		return new StudentDto(studentEntity.getId(), studentEntity.getIndexNumber(), studentEntity.getFirstname(), studentEntity.getLastname(), studentEntity.getEmail(), studentEntity.getAdress(), cityConverter.entityToDto(studentEntity.getCityEntity()), studentEntity.getPhone(), studentEntity.getCurrentYearOfStudy());
	}
	public StudentEntity dtoToEntity(StudentDto studentDto) {
		return new StudentEntity(studentDto.getId(), studentDto.getIndexNumber(), studentDto.getFirstname(), studentDto.getLastname(), studentDto.getEmail(), studentDto.getAdress(), cityConverter.dtoToEntity(studentDto.getCityDto()), studentDto.getPhone(), studentDto.getCurrentYearOfStudy());
	}
	public List<StudentEntity> dtoToEntityList(List<StudentDto> dtolist) {
		List<StudentEntity> entityList = new ArrayList<StudentEntity>();
		for(StudentDto studentDto: dtolist) {
			entityList.add(dtoToEntity(studentDto));
		}
		return entityList;
	}
	public List<StudentDto> entityToDtoList(List<StudentEntity> entitylist) {
		List<StudentDto> dtolist = new ArrayList<StudentDto>();
		for(StudentEntity studentEntity: entitylist) {
			dtolist.add(entityToDto(studentEntity));
		}
		return dtolist;
	}
}
