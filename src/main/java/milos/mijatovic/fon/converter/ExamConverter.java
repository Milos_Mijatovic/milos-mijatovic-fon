package milos.mijatovic.fon.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import milos.mijatovic.fon.dto.ExamDto;
import milos.mijatovic.fon.entity.ExamEntity;

@Component
public class ExamConverter {
	private final ProfessorConverter professorConverter;
	private final SubjectConverter subjectConverter;
		
		@Autowired
		public ExamConverter( @Lazy ProfessorConverter professorConverter, @Lazy SubjectConverter subjectConverter) {
			this.professorConverter = professorConverter;
			this.subjectConverter = subjectConverter;
		}
		
		
		public ExamDto entityToDto(ExamEntity examEntity) {
			return new ExamDto(examEntity.getId(),  subjectConverter.entityToDto(examEntity.getSubject()), professorConverter.entityToDto(examEntity.getProfessor()), examEntity.getDate()); // ovde vrti u beskonacno
		}
		public ExamEntity dtoToEntity(ExamDto examDto) {
			return new ExamEntity(examDto.getId(),  subjectConverter.dtoToEntity(examDto.getSubjectDto()), professorConverter.dtoToEntity(examDto.getProfessorDto()), examDto.getDate());
		}
		public List<ExamEntity> dtoToEntityList(List<ExamDto> dtolist) {
			List<ExamEntity> entityList = new ArrayList<ExamEntity>();
			for(ExamDto examDto: dtolist) {
				entityList.add(dtoToEntity(examDto));
			}
			return entityList;
		}
		public List<ExamDto> entityToDtoList(List<ExamEntity> entitylist) {
			List<ExamDto> dtolist = new ArrayList<ExamDto>();
			for(ExamEntity examEntity: entitylist) {
				dtolist.add(entityToDto(examEntity));
			}
			return dtolist;
		}
	

}
