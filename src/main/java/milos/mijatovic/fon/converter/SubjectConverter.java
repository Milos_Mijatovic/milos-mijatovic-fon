package milos.mijatovic.fon.converter;




import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import milos.mijatovic.fon.dto.SubjectDto;
import milos.mijatovic.fon.entity.SubjectEntity;
@Component
public class SubjectConverter {
	
	
	

	public SubjectDto entityToDto(SubjectEntity subjectEntity) {
		
		return new SubjectDto(subjectEntity.getId(), subjectEntity.getName(), subjectEntity.getDescription(), subjectEntity.getYearOfStudy(), subjectEntity.getSemester()); 
	}
	public SubjectEntity dtoToEntity(SubjectDto subjectDto) {
		return new SubjectEntity(subjectDto.getId(), subjectDto.getName(), subjectDto.getDescription(), subjectDto.getYearOfStudy(), subjectDto.getSemester()); 
	}
	public List<SubjectEntity> dtoToEntityList(List<SubjectDto> dtolist) {
		List<SubjectEntity> entityList = new ArrayList<SubjectEntity>();
		for(SubjectDto subjectDto: dtolist) {
			entityList.add(dtoToEntity(subjectDto));
		}
		return entityList;
	}
	public List<SubjectDto> entityToDtoList(List<SubjectEntity> entitylist) {
		List<SubjectDto> dtolist = new ArrayList<SubjectDto>();
		for(SubjectEntity subjectEntity: entitylist) {
			dtolist.add(entityToDto(subjectEntity));
		}
		return dtolist;
	}
	

	
}
