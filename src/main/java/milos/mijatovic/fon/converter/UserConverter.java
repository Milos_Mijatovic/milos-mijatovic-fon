package milos.mijatovic.fon.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import milos.mijatovic.fon.dto.UserDto;
import milos.mijatovic.fon.entity.UserEntity;

@Component
public class UserConverter {
	public UserDto entityToDto(UserEntity userEntity) {
		return new UserDto(userEntity.getId(), userEntity.getUsername(), userEntity.getPassword()); 
	}
	public UserEntity dtoToEntity(UserDto userDto) {
		return new UserEntity(userDto.getId(), userDto.getUsername(), userDto.getPassword()); 
	}
	public List<UserEntity> dtoToEntityList(List<UserDto> dtolist) {
		List<UserEntity> entityList = new ArrayList<UserEntity>();
		for(UserDto userDto: dtolist) {
			entityList.add(dtoToEntity(userDto));
		}
		return entityList;
	}
	public List<UserDto> entityToDtoList(List<UserEntity> entitylist) {
		List<UserDto> dtolist = new ArrayList<UserDto>();
		for(UserEntity userEntity: entitylist) {
			dtolist.add(entityToDto(userEntity));
		}
		return dtolist;
	}
}
