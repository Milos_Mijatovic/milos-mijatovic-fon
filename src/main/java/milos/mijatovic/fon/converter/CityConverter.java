package milos.mijatovic.fon.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import milos.mijatovic.fon.dto.CityDto;
import milos.mijatovic.fon.entity.CityEntity;

@Component
public class CityConverter {
	public CityDto entityToDto(CityEntity cityEntity) {
		if(cityEntity == null) {
			return null;
		}
		return new CityDto(cityEntity.getId(), cityEntity.getCityNumber(), cityEntity.getName()); 
	}
	public CityEntity dtoToEntity(CityDto cityDto) {
		if(cityDto==null) {
			return null;
		}
		return new CityEntity(cityDto.getId(), cityDto.getCityNumber(), cityDto.getName()); 
	}
	public List<CityEntity> dtoToEntityList(List<CityDto> dtolist) {
		List<CityEntity> entityList = new ArrayList<CityEntity>();
		for(CityDto cityDto: dtolist) {
			entityList.add(dtoToEntity(cityDto));
		}
		return entityList;
	}
	public List<CityDto> entityToDtoList(List<CityEntity> entitylist) {
		List<CityDto> dtolist = new ArrayList<CityDto>();
		for(CityEntity cityEntity: entitylist) {
			dtolist.add(entityToDto(cityEntity));
		}
		return dtolist;
	}
}
