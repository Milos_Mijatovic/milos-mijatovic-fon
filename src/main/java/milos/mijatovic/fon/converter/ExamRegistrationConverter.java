package milos.mijatovic.fon.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import milos.mijatovic.fon.dto.ExamRegistrationDto;
import milos.mijatovic.fon.entity.ExamRegistrationEntity;


@Component
public class ExamRegistrationConverter {
	private final ExamConverter examConverter;
	private final StudentConverter studentConverter;
		
		@Autowired
		public ExamRegistrationConverter( @Lazy ExamConverter examConverter, @Lazy StudentConverter studentConverter) {
			this.examConverter = examConverter;
			this.studentConverter = studentConverter;
		}
		
		
		public ExamRegistrationDto entityToDto(ExamRegistrationEntity examRegistrationEntity) {
			return new ExamRegistrationDto(examRegistrationEntity.getId(),  examConverter.entityToDto(examRegistrationEntity.getExamEntity()), studentConverter.entityToDto(examRegistrationEntity.getStudentEntity()));
		}
		public ExamRegistrationEntity dtoToEntity(ExamRegistrationDto examRegistrationDto) {
			return new ExamRegistrationEntity(examRegistrationDto.getId(),  examConverter.dtoToEntity(examRegistrationDto.getExamDto()), studentConverter.dtoToEntity(examRegistrationDto.getStudentDto()));
		}
		public List<ExamRegistrationEntity> dtoToEntityList(List<ExamRegistrationDto> dtolist) {
			List<ExamRegistrationEntity> entityList = new ArrayList<ExamRegistrationEntity>();
			for(ExamRegistrationDto examRegistrationDto: dtolist) {
				entityList.add(dtoToEntity(examRegistrationDto));
			}
			return entityList;
		}
		public List<ExamRegistrationDto> entityToDtoList(List<ExamRegistrationEntity> entitylist) {
			List<ExamRegistrationDto> dtolist = new ArrayList<ExamRegistrationDto>();
			for(ExamRegistrationEntity examRegistrationEntity: entitylist) {
				dtolist.add(entityToDto(examRegistrationEntity));
			}
			return dtolist;
		}
}
