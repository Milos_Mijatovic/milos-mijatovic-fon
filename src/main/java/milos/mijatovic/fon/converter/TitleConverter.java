package milos.mijatovic.fon.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import milos.mijatovic.fon.dto.TitleDto;
import milos.mijatovic.fon.entity.TitleEntity;

@Component
public class TitleConverter {
	public TitleDto entityToDto(TitleEntity titleEntity) {
		return new TitleDto(titleEntity.getId(), titleEntity.getName()); 
	}
	public TitleEntity dtoToEntity(TitleDto titleDto) {
		return new TitleEntity(titleDto.getId(), titleDto.getName()); 
	}
	public List<TitleEntity> dtoToEntityList(List<TitleDto> dtolist) {
		List<TitleEntity> entityList = new ArrayList<TitleEntity>();
		for(TitleDto titleDto: dtolist) {
			entityList.add(dtoToEntity(titleDto));
		}
		return entityList;
	}
	public List<TitleDto> entityToDtoList(List<TitleEntity> entitylist) {
		List<TitleDto> dtolist = new ArrayList<TitleDto>();
		for(TitleEntity titleEntity: entitylist) {
			dtolist.add(entityToDto(titleEntity));
		}
		return dtolist;
	}
}
