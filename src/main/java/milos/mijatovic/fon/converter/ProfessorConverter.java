package milos.mijatovic.fon.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import milos.mijatovic.fon.dto.ProfessorDto;
import milos.mijatovic.fon.entity.ProfessorEntity;

@Component
public class ProfessorConverter {
private final CityConverter cityConverter;
private final TitleConverter titleConverter;

	
	@Autowired
	public ProfessorConverter(CityConverter cityConverter, TitleConverter titleConverter) {
		this.cityConverter = cityConverter;
		this.titleConverter = titleConverter;
	}
	
	public ProfessorDto entityToDto(ProfessorEntity professorEntity) {
		return new ProfessorDto(professorEntity.getId(), professorEntity.getFirstname(), professorEntity.getLastname(), professorEntity.getEmail(), professorEntity.getAdress(), cityConverter.entityToDto(professorEntity.getCity()), professorEntity.getPhone(), professorEntity.getReelectionDate(), titleConverter.entityToDto(professorEntity.getTitle()));
	}
	public ProfessorEntity dtoToEntity(ProfessorDto professorDto) {
		return new ProfessorEntity(professorDto.getId(), professorDto.getFirstname(), professorDto.getLastname(), professorDto.getEmail(), professorDto.getAdress(), cityConverter.dtoToEntity(professorDto.getCityDto()), professorDto.getPhone(), professorDto.getReelectionDate(), titleConverter.dtoToEntity(professorDto.getTitleDto()));
	}
	public List<ProfessorEntity> dtoToEntityList(List<ProfessorDto> dtolist) {
		List<ProfessorEntity> entityList = new ArrayList<ProfessorEntity>();
		for(ProfessorDto professorDto: dtolist) {
			entityList.add(dtoToEntity(professorDto));
		}
		return entityList;
	}
	public List<ProfessorDto> entityToDtoList(List<ProfessorEntity> entitylist) {
		List<ProfessorDto> dtolist = new ArrayList<ProfessorDto>();
		for(ProfessorEntity professorEntity: entitylist) {
			dtolist.add(entityToDto(professorEntity));
		}
		return dtolist;
	}
	
	
}
