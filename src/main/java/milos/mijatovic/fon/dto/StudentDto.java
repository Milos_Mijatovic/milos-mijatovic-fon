package milos.mijatovic.fon.dto;


import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;


public class StudentDto implements Serializable{

	
	
	private static final long serialVersionUID = 1L;
	private Long id;
	@Length(min = 10, max=10, message = "Minimum number of characters is 10")
	@NotNull(message = "Minimum number of characters is 10")
	private String indexNumber;
	@Length(min = 3, message = "Minimum number of characters is 3")
	@NotNull(message = "Minimum number of characters is 3")
	private String firstname;
	@Length(min = 3, message = "Minimum number of characters is 3")
	@NotNull(message = "Minimum number of characters is 3")
	private String lastname;
	private String email;
	private String adress;
	private CityDto cityDto;
	@Length(max = 15, message = "Max number of characters is 15")
	private String phone;
	@Min(1)
	private int currentYearOfStudy;
	public StudentDto(Long id, String indexNumber, String firstname, String lastname, String email, String adress,
			CityDto cityDto, String phone, int currentYearOfStudy) {
		super();
		this.id = id;
		this.indexNumber = indexNumber;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.adress = adress;
		this.cityDto = cityDto;
		this.phone = phone;
		this.currentYearOfStudy = currentYearOfStudy;
	}
	public StudentDto() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIndexNumber() {
		return indexNumber;
	}
	public void setIndexNumber(String indexNumber) {
		this.indexNumber = indexNumber;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public CityDto getCityDto() {
		return cityDto;
	}
	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getCurrentYearOfStudy() {
		return currentYearOfStudy;
	}
	public void setCurrentYearOfStudy(int currentYearOfStudy) {
		this.currentYearOfStudy = currentYearOfStudy;
	}
	@Override
	public String toString() {
		return "StudentDto [id=" + id + ", indexNumber=" + indexNumber + ", firstname=" + firstname + ", lastname="
				+ lastname + ", email=" + email + ", adress=" + adress + ", cityDto=" + cityDto + ", phone=" + phone
				+ ", currentYearOfStudy=" + currentYearOfStudy + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((indexNumber == null) ? 0 : indexNumber.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentDto other = (StudentDto) obj;
		if (indexNumber == null) {
			if (other.indexNumber != null)
				return false;
		} else if (!indexNumber.equals(other.indexNumber))
			return false;
		return true;
	}
	public String getFullname() {
		return firstname + " " + lastname;
	}
	
	
	
}
