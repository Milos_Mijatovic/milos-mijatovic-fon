package milos.mijatovic.fon.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

public class ExamDto implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	@NotNull(message = "Must choose subject")
	private SubjectDto subjectDto;
	@NotNull(message = "Must choose professor")
	private ProfessorDto professorDto;
	@NotNull(message = "Must choose a date")
	private Date date;
	
	public ExamDto() {
		
	}

	public ExamDto(Long id, SubjectDto subjectDto, ProfessorDto professorDto, Date date) {
		super();
		this.id = id;
		this.subjectDto = subjectDto;
		this.professorDto = professorDto;
		this.date = date;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SubjectDto getSubjectDto() {
		return subjectDto;
	}

	public void setSubjectDto(SubjectDto subjectDto) {
		this.subjectDto = subjectDto;
	}

	public ProfessorDto getProfessorDto() {
		return professorDto;
	}

	public void setProfessorDto(ProfessorDto professorDto) {
		this.professorDto = professorDto;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "ExamDto [id=" + id + ", subjectDto=" + subjectDto + ", professorDto=" + professorDto + ", date=" + date
				+ "]";
	}
	
	

}
