package milos.mijatovic.fon.dto;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import milos.mijatovic.fon.enums.Semester;

public class SubjectDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	@Length(min = 3, message = "Minimum number of characters is 3")
	@NotNull(message = "Minimum number of characters is 3")
	private String name;
	private String description;
	@Min(1)
	private int yearOfStudy;
	private Semester semester;
	
	
	public SubjectDto() {
	
	}
	
	

	public SubjectDto(Long id, String name, String description, int yearOfStudy, Semester semester
			) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.yearOfStudy = yearOfStudy;
		this.semester = semester;
	
	}



	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getYearOfStudy() {
		return yearOfStudy;
	}
	public void setYearOfStudy(int yearOfStudy) {
		this.yearOfStudy = yearOfStudy;
	}
	public Semester getSemester() {
		return semester;
	}
	public void setSemester(Semester semester) {
		this.semester = semester;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	


	@Override
	public String toString() {
		return "SubjectDto [id=" + id + ", name=" + name + ", description=" + description + ", yearOfStudy="
				+ yearOfStudy + ", semester=" + semester + "]";
	}
	
	
}
