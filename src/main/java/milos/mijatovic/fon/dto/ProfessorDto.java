package milos.mijatovic.fon.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;


import org.hibernate.validator.constraints.Length;



public class ProfessorDto  implements Serializable {

	

	private static final long serialVersionUID = 1L;
	private Long id;
	@Length(min = 3, message = "Minimum number of characters is 3")
	@NotNull(message = "Minimum number of characters is 3")
	private String firstname;
	@Length(min = 3, message = "Minimum number of characters is 3")
	@NotNull(message = "Minimum number of characters is 3")
	private String lastname;
	private String email;
	private String adress;
	private CityDto cityDto;
	@Length(max = 15, message = "Max number of characters is 15")
	private String phone;
	@NotNull(message = "Must choose a date")
	private Date reelectionDate;
	private TitleDto titleDto;
	

	
	
	public ProfessorDto(Long id, String firstname, String lastname, String email, String adress, CityDto cityDto,
			String phone, Date reelectionDate, TitleDto titleDto) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.adress = adress;
		this.cityDto = cityDto;
		this.phone = phone;
		this.reelectionDate = reelectionDate;
		this.titleDto = titleDto;

	}
	public ProfessorDto() {
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAdress() {
		return adress;
	}
	public void setAdress(String adress) {
		this.adress = adress;
	}
	public CityDto getCityDto() {
		return cityDto;
	}
	public void setCityDto(CityDto cityDto) {
		this.cityDto = cityDto;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Date getReelectionDate() {
		return reelectionDate;
	}
	public void setReelectionDate(Date reelectionDate) {
		this.reelectionDate = reelectionDate;
	}
	public TitleDto getTitleDto() {
		return titleDto;
	}
	public void setTitleDto(TitleDto titleDto) {
		this.titleDto = titleDto;
	}
	

	@Override
	public String toString() {
		return "ProfessorDto [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email
				+ ", adress=" + adress + ", cityDto=" + cityDto + ", phone=" + phone + ", reelectionDate="
				+ reelectionDate + ", titleDto=" + titleDto + "]";
	}
	
	public String getFullname() {
		return firstname + " " + lastname;
	}
	
	
	
}
