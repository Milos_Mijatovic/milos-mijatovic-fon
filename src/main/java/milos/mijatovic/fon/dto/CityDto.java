package milos.mijatovic.fon.dto;

import java.io.Serializable;

public class CityDto implements Serializable {
	

	private static final long serialVersionUID = 1L;
	private Long id;
	private int cityNumber;
	private String name;
	public CityDto(Long id, int cityNumber, String name) {
		super();
		this.id = id;
		this.cityNumber = cityNumber;
		this.name = name;
	}
	public CityDto() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getCityNumber() {
		return cityNumber;
	}
	public void setCityNumber(int cityNumber) {
		this.cityNumber = cityNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "CityDto [id=" + id + ", cityNumber=" + cityNumber + ", name=" + name + "]";
	}
	
	
}
