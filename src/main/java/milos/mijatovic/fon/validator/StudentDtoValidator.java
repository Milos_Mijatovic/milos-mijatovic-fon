package milos.mijatovic.fon.validator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import milos.mijatovic.fon.dto.ProfessorDto;
import milos.mijatovic.fon.dto.StudentDto;
import milos.mijatovic.fon.service.ProfessorService;
import milos.mijatovic.fon.service.StudentService;

public class StudentDtoValidator implements Validator{

	private final StudentService studentService;
	private final ProfessorService professorService;
	
	@Autowired
	public StudentDtoValidator(StudentService studentService, ProfessorService professorService) {
		this.studentService = studentService;
		this.professorService = professorService;
	}
	@Override
	public boolean supports(Class<?> clazz) {
		return StudentDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		StudentDto studentDto = (StudentDto) target;
		List<StudentDto> duplicateIndexNumber = studentService.findByIndexNumber(studentDto);
		List<StudentDto> duplicateStudentEmail = studentService.findByEmail(studentDto);
		List<ProfessorDto> duplicateProfEmail = professorService.findByEmail(studentDto.getEmail());
		if(!duplicateIndexNumber.isEmpty() && duplicateIndexNumber.get(0).getId() != studentDto.getId()) {
			errors.rejectValue("indexNumber", "StudentDto.indexNumber", "IndexNumber already exists");
		}
		
		
		if(studentDto.getEmail()!=null) {
			if(!studentDto.getEmail().contains("@")) {
				errors.rejectValue("email", "StudentDto.email", 
						"Must have @ character");
			}
		}

		if(studentDto.getAdress()!= null) {
			if(studentDto.getAdress().trim().length() < 3) {
				errors.rejectValue("adress", "StudentDto.adress", 
						"Minimal number of characters is 3");
			}
		}
		if(studentDto.getPhone()!= null) {
			if(studentDto.getPhone().trim().length() < 6) {
				errors.rejectValue("phone", "StudentDto.phone", 
						"Minimal number of characters is 6");
			}
		}
		if(studentDto.getEmail()!= null) {
			if(!duplicateStudentEmail.isEmpty() && !duplicateStudentEmail.get(0).getId().equals(studentDto.getId()) || !duplicateProfEmail.isEmpty()) {
				errors.rejectValue("email", "StudentDto.email", "Email already exists");
			}

		}
	
	}

}
