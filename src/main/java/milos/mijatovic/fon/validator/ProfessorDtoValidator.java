package milos.mijatovic.fon.validator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import milos.mijatovic.fon.dto.ProfessorDto;
import milos.mijatovic.fon.dto.StudentDto;
import milos.mijatovic.fon.service.ProfessorService;
import milos.mijatovic.fon.service.StudentService;

public class ProfessorDtoValidator implements Validator{
	ProfessorService professorService;
	StudentService studentService;
	
	@Autowired
	public ProfessorDtoValidator(ProfessorService professorService, StudentService studentService) {
		this.professorService = professorService;
		this.studentService = studentService;
	}
	@Override
	public boolean supports(Class<?> clazz) {
		return ProfessorDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ProfessorDto professorDto = (ProfessorDto) target;
		List<ProfessorDto> duplicateProfEmail = professorService.findByEmail(professorDto);
		List<StudentDto> duplicateStudentEmail = studentService.findByEmail(professorDto.getEmail());
		if(professorDto.getEmail()!=null) {
			if(!professorDto.getEmail().contains("@")) {
				errors.rejectValue("email", "ProfessorDto.email", 
						"Must have @ character");
			}
		}
		if(professorDto.getAdress()!= null) {
			if(professorDto.getAdress().trim().length() < 3) {
				errors.rejectValue("adress", "ProfessorDto.adress", 
						"Minimal number of characters is 3");
			}
			
		}
		if(professorDto.getPhone()!=null) {
			if(professorDto.getPhone().trim().length() < 6) {
				errors.rejectValue("phone", "ProfessorDto.phone", 
						"Minimal number of characters is 6");
			}
		} 
		if(professorDto.getEmail()!= null) {
			if(!duplicateProfEmail.isEmpty() && !duplicateProfEmail.get(0).getId().equals(professorDto.getId()) || !duplicateStudentEmail.isEmpty()) {
				errors.rejectValue("email", "ProfessorDto.email", "Email already exists");
			}
			
		}
	}

}
