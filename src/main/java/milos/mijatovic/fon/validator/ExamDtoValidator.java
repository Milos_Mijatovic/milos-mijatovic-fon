package milos.mijatovic.fon.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import milos.mijatovic.fon.dto.ExamDto;
import milos.mijatovic.fon.service.ExamService;

public class ExamDtoValidator implements Validator{

	private final SimpleDateFormat simpleDateFormat;
	private final ExamService examService;
	@Autowired
	public ExamDtoValidator(SimpleDateFormat simpleDateFormat, ExamService examService) {
		this.simpleDateFormat = simpleDateFormat;
		this.examService = examService;
	}
	
	@Override
	public boolean supports(Class<?> clazz) {
		return ExamDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ExamDto examDto  = (ExamDto) target;
		Date currentDate = new Date(); 
		Date formattedDate;
		if(examDto.getDate() !=null) {
		try {
			formattedDate = simpleDateFormat.parse((simpleDateFormat.format((currentDate))));
			Date tomorrow = new Date(formattedDate.getTime() + (1000 * 60 * 60 * 24));
			System.out.println(formattedDate);
			if(examDto.getDate().before(tomorrow)) {
				errors.rejectValue("date", "ExamDto.class", "Must choose date after: " + currentDate.toString());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		}
		if(examDto.getDate() !=null) {
		if(!examService.findAfterDate(examDto).isEmpty()) {
			errors.rejectValue("subjectDto", "ExamDto.class", "There is already active exam for that subject");
		}
		}
		
		if(examDto.getDate() == null) {
			errors.rejectValue("date", "ExamDto.class", "Must choose date after: " + currentDate.toString());
		}
			
		
	}

}
