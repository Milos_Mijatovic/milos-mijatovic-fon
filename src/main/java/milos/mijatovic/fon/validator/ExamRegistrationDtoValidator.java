package milos.mijatovic.fon.validator;


import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import milos.mijatovic.fon.dto.ExamRegistrationDto;

public class ExamRegistrationDtoValidator implements Validator {

	
	@Override
	public boolean supports(Class<?> clazz) {
		return ExamRegistrationDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ExamRegistrationDto examRegistrationDto = (ExamRegistrationDto) target;
		
		if(examRegistrationDto.getStudentDto() != null && examRegistrationDto.getExamDto() != null) {
		if(examRegistrationDto.getStudentDto().getCurrentYearOfStudy() < examRegistrationDto.getExamDto().getSubjectDto().getYearOfStudy()) {
			errors.rejectValue("studentDto", "ExamRegistrationDto.studentDto","Cannot register, student current year of study is lower than chosen subject"); 
		
		}
		
		
		
	}

}
}
