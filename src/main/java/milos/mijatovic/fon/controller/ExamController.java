package milos.mijatovic.fon.controller;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import milos.mijatovic.fon.dto.ExamDto;
import milos.mijatovic.fon.dto.ProfessorDto;
import milos.mijatovic.fon.dto.SubjectDto;
import milos.mijatovic.fon.service.ExamService;
import milos.mijatovic.fon.service.ProfessorService;
import milos.mijatovic.fon.service.SubjectService;
import milos.mijatovic.fon.validator.ExamDtoValidator;

@Controller
@RequestMapping(value = "/exam")
public class ExamController {
	private final ExamService examService;
	private final SubjectService subjectService;
	private final ProfessorService professorService;
	private final SimpleDateFormat simpleDateFormat;
	
	@Autowired
	public ExamController(SubjectService subjectService, ProfessorService professorService, ExamService examService, SimpleDateFormat simpleDateFormat) {
		this.subjectService = subjectService;
		this.professorService = professorService;
		this.examService = examService;
		this.simpleDateFormat = simpleDateFormat;
	}
	
	@GetMapping(value = "/home")
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("/exam/home");
		return modelAndView;
	}
	@GetMapping(value = "/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("/exam/add");
		return modelAndView;
	}
	@PostMapping(value = "/save")
	public ModelAndView save(@Valid @ModelAttribute ("examDto") ExamDto examDto, Errors errors) {
		ModelAndView modelAndView = new ModelAndView();
		if(errors.hasErrors()) {
			modelAndView.setViewName("/exam/add");
			modelAndView.addObject("examDto", examDto);
		}
		else {
			examService.save(examDto);
			modelAndView.setViewName("redirect:/exam/all");
		}
		return modelAndView;
	}
	@GetMapping(value = "/all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("/exam/all");
		return modelAndView;
	}
	
	
	@ModelAttribute(name = "subjects")
	public List<SubjectDto> subjects(){
		return subjectService.getAll();
	}
	@ModelAttribute(name = "professors")
	public List<ProfessorDto> professors(){
		return professorService.getAll();
	}
	
	@ModelAttribute(name="examDto")
	public ExamDto examDto() {
		return new ExamDto();
	}
	@ModelAttribute(name="exams")
	public List<ExamDto> exams(){
		return examService.getAll();
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new ExamDtoValidator(simpleDateFormat, examService));
	}
}
