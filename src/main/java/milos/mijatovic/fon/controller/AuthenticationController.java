package milos.mijatovic.fon.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;


import milos.mijatovic.fon.dto.UserDto;
import milos.mijatovic.fon.exception.UserValidationException;
import milos.mijatovic.fon.service.UserService;


@Controller
@RequestMapping(value = "/authentication")
public class AuthenticationController {

	private final UserService userService;
	
	@Autowired
	public AuthenticationController(UserService userService) {
		this.userService = userService;
	}
	
	@PostMapping(value = "/login")
	public ModelAndView confirm(@Valid @ModelAttribute ("userDto") UserDto userDto, Errors errors, HttpServletRequest request) throws UserValidationException {
		ModelAndView modelAndView = new ModelAndView();
		userService.findUserByNameAndPassword(userDto);
		if(errors.hasErrors()) {
			modelAndView.setViewName("/authentication/login");
			
			modelAndView.addObject("userDto", userDto);
		} else {
			
			HttpSession session = request.getSession(false);
			session.setAttribute("user", userDto);
			modelAndView.setViewName("redirect:/home/home");
		}
		return modelAndView;
	}
	@GetMapping(value = "/login")
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView("/authentication/login");
		modelAndView.addObject("userDto", new UserDto());
		return modelAndView;
	}
	@GetMapping(value = "/logout")
	public ModelAndView logout(HttpServletRequest request, SessionStatus status) {
	    HttpSession session = request.getSession(false);
	    if (session != null) {
	        session.invalidate();
	        status.setComplete();
	    }
	    return new ModelAndView("redirect:/authentication/login");
	}
	
	@ModelAttribute(name = "userDto")
	public UserDto userDto() {
		return new UserDto();
	}
	
	@ExceptionHandler(UserValidationException.class)
	public ModelAndView exceptionHandler(UserValidationException userValidationException) {
		ModelAndView modelAndView = new ModelAndView("/authentication/login");
		modelAndView.addObject("errorMessage", userValidationException.getMessage());
		modelAndView.addObject("userDto", userDto());
		return modelAndView;
	}
}
