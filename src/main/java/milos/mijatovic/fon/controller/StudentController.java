package milos.mijatovic.fon.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import milos.mijatovic.fon.dto.CityDto;
import milos.mijatovic.fon.dto.ExamRegistrationDto;
import milos.mijatovic.fon.dto.StudentDto;
import milos.mijatovic.fon.exception.DataNotFoundException;
import milos.mijatovic.fon.service.CityService;
import milos.mijatovic.fon.service.ExamRegistrationService;
import milos.mijatovic.fon.service.ProfessorService;
import milos.mijatovic.fon.service.StudentService;
import milos.mijatovic.fon.validator.StudentDtoValidator;


@Controller
@RequestMapping(value = "/student")
public class StudentController {
	
	
	private final CityService cityService;
	private final StudentService studentService;
	private final ProfessorService professorService;
	private final ExamRegistrationService examRegistrationService;
	
	@Autowired
	public StudentController(StudentService studentService, CityService cityService, ProfessorService professorService, ExamRegistrationService examRegistrationService) {
		this.studentService = studentService;
		this.cityService = cityService;
		this.professorService = professorService;
		this.examRegistrationService = examRegistrationService;
	}
	
	
	
	@GetMapping
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("/student/home");
		return modelAndView;
	}
	
	@GetMapping(value = "/all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("/student/all");
		return modelAndView;
	}
	@GetMapping(value = "/add")
	public ModelAndView add(SessionStatus sessionStatus) {
		sessionStatus.setComplete();
		ModelAndView modelAndView = new ModelAndView("/student/add");
		return modelAndView;
	}
	@PostMapping(value = "/save")
	public ModelAndView save(@Valid
			@ModelAttribute("studentDto") StudentDto studentDto, Errors erros, SessionStatus sessionStatus) {
		ModelAndView modelAndView = new ModelAndView();
		if(erros.hasErrors()) {
			modelAndView.setViewName("/student/add");
			modelAndView.addObject("studentDto", studentDto);
			System.out.println(studentDto);
		}
		else {
			studentService.save(studentDto);
			sessionStatus.setComplete();
			modelAndView.setViewName("redirect:/student/all");
		}
		
		return modelAndView;
	}
	@GetMapping(value = "/edit")
	public ModelAndView edit(@RequestParam ("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("/student/edit");
		modelAndView.addObject("studentDto", studentService.findById(id));
		return modelAndView;
	}
	@PostMapping(value = "/update")
	public ModelAndView update(@Valid @ModelAttribute ("studentDto") StudentDto studentDto, Errors errors, SessionStatus sessionStatus) {
		ModelAndView modelAndView = new ModelAndView();
		if(errors.hasErrors()) {
			modelAndView.setViewName("/student/edit");
			modelAndView.addObject("studentDto", studentDto);
		}
		else {
		studentService.save(studentDto);
		sessionStatus.setComplete();
		modelAndView.setViewName("redirect:/student/all");
		}
		return modelAndView;
	}
	
	@GetMapping("/remove")
	public ModelAndView remove(@RequestParam("id") Long id) throws DataNotFoundException {
		ModelAndView modelAndView = new ModelAndView("redirect:/student/all");
		StudentDto studentDto = studentService.findById(id);
		examRegistrationService.findByStudent(studentDto);
		studentService.deleteById(id);
		return modelAndView;
	}
	@GetMapping("/details")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("/student/details");
		StudentDto studentDto = studentService.findById(id);
		List<ExamRegistrationDto> examRegistrations = examRegistrationService.currentlyRegisteredExams(id);
		modelAndView.addObject("studentDto", studentDto);
		modelAndView.addObject("examRegistrations", examRegistrations);
		return modelAndView;
	}
	
	@ModelAttribute(value="students")
	public List<StudentDto> students(){
		return studentService.getAll();
	}
	@ModelAttribute(value="studentDto")
	public StudentDto studentDto() {
		return new StudentDto();
	}
	@ModelAttribute(value = "cities")
	public List<CityDto> cities(){
	
		return cityService.getAll();
	}
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new StudentDtoValidator(studentService, professorService));
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	@ExceptionHandler(DataNotFoundException.class)
	public ModelAndView dataNotFound(DataNotFoundException dataNotFoundException) {
		ModelAndView modelAndView = new ModelAndView("/student/all");
		modelAndView.addObject("errorMessage", dataNotFoundException.getMessage());
		modelAndView.addObject("students", students());
		return modelAndView;
	}
}
