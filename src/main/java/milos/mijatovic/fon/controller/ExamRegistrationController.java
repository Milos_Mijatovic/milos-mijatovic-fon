package milos.mijatovic.fon.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import milos.mijatovic.fon.dto.ExamDto;
import milos.mijatovic.fon.dto.ExamRegistrationDto;
import milos.mijatovic.fon.dto.StudentDto;
import milos.mijatovic.fon.exception.DataNotFoundException;
import milos.mijatovic.fon.service.ExamRegistrationService;
import milos.mijatovic.fon.service.ExamService;
import milos.mijatovic.fon.service.StudentService;
import milos.mijatovic.fon.validator.ExamRegistrationDtoValidator;

@Controller
@RequestMapping(value = "/examRegistration")
public class ExamRegistrationController {

	private final ExamRegistrationService examRegistrationService;
	private final ExamService examService;
	private final StudentService studentService;

	
	@Autowired
	public ExamRegistrationController(ExamRegistrationService examRegistrationService, ExamService examService, StudentService studentService) {
		this.examRegistrationService = examRegistrationService;
		this.examService = examService;
		this.studentService = studentService;
	}
	
	@GetMapping(value = "/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("/examRegistration/add");
		return modelAndView;
	}
	@PostMapping(value = "/save")
	public ModelAndView save(@Valid @ModelAttribute ("examRegistrationDto") ExamRegistrationDto examRegExamDto, Errors errors) throws DataNotFoundException {
		ModelAndView modelAndView = new ModelAndView();
		if(examRegExamDto.getExamDto()!=null && examRegExamDto.getStudentDto()!=null) {
		examRegistrationService.findAllExamsAfterDateByName(examRegExamDto);
		}
		System.out.println("EXAM ERRORS");
		if(errors.hasErrors()) {
			System.out.println("USAO");
			modelAndView.setViewName("/examRegistration/add");
			modelAndView.addObject("examRegistrationDto", examRegExamDto);
			System.out.println("ISPIS");
			System.out.println(examRegExamDto);
			System.out.println("================================");
			
		}
		else {
			examRegistrationService.save(examRegExamDto);
			modelAndView.setViewName("redirect:/examRegistration/all");
		}
		return modelAndView;
	}
	@GetMapping(value = "/all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("/examRegistration/all");
		return modelAndView;
	}
	
	
	@ModelAttribute(name = "exams")
	public List<ExamDto> exams(){
		return examService.findExamsWeekBefore();
	}
	@ModelAttribute(name = "students")
	public List<StudentDto> students(){
		return studentService.getAll();
	}
	
	@ModelAttribute(name="examRegistrationDto")

	public ExamRegistrationDto examDto() {
		return new ExamRegistrationDto();
	}
	
	@ModelAttribute(name = "examregs")
	public List<ExamRegistrationDto> examregs(){
		return examRegistrationService.getAll();
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new ExamRegistrationDtoValidator());
	}
	@ExceptionHandler(DataNotFoundException.class)
	public ModelAndView exceptionHandler(DataNotFoundException dataNotFoundException) {
		ModelAndView modelAndView = new ModelAndView("/examRegistration/add");
		modelAndView.addObject("errorMessage", dataNotFoundException.getMessage());
		modelAndView.addObject("examRegistrationDto", (ExamRegistrationDto)dataNotFoundException.getObject());
		modelAndView.addObject("students", students());
		modelAndView.addObject("exams", exams());
		return modelAndView;
	}
}
