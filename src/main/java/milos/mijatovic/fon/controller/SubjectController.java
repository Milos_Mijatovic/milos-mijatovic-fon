package milos.mijatovic.fon.controller;

import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import milos.mijatovic.fon.dto.SubjectDto;
import milos.mijatovic.fon.enums.Semester;
import milos.mijatovic.fon.exception.DataNotFoundException;
import milos.mijatovic.fon.service.ExamService;
import milos.mijatovic.fon.service.SubjectService;

@Controller
@RequestMapping(value = "/subject")
public class SubjectController {
	
	private final SubjectService subjectService;
	private final ExamService examService;
	
	@Autowired
	public SubjectController(SubjectService subjectService, ExamService examService) {
		this.subjectService = subjectService;
		this.examService = examService;
	}
	
	@GetMapping
	public String home() {
		return "subject/home";
	}
	
	@GetMapping("/all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("/subject/all");
		return modelAndView;
	}
	@GetMapping("/add")
	public ModelAndView add() {
		ModelAndView modelAndView = new ModelAndView("/subject/add");
		return modelAndView;
	}
	@GetMapping("/remove")
	public ModelAndView remove(@RequestParam("id") Long id) throws DataNotFoundException {
		ModelAndView modelAndView = new ModelAndView("redirect:/subject/all");
		SubjectDto subjectDto = subjectService.findById(id);
		examService.findBySubject(subjectDto);
		subjectService.deleteById(id);
		return modelAndView;
	}
	@PostMapping("/save")
	public ModelAndView save(@Valid @ModelAttribute("subjectDto") SubjectDto subjectDto, Errors errors) {
		ModelAndView modelAndView = new ModelAndView();
		if(errors.hasErrors()) {
			modelAndView.setViewName("/subject/add");
			modelAndView.addObject("subjectDto", subjectDto);
		}
		else {
		subjectService.save(subjectDto);
		modelAndView.setViewName("redirect:/subject/all");
		}
		return modelAndView;
	}
	@GetMapping("/edit")
	public ModelAndView edit(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("/subject/edit");
		modelAndView.addObject("subjectDto", subjectService.findById(id));
		return modelAndView;
	}
	
	@PostMapping("/update")
	public ModelAndView update(@Valid @ModelAttribute("subjectDto") SubjectDto subjectDto, Errors errors) {
		ModelAndView modelAndView = new ModelAndView();
		if(errors.hasErrors()) {
			modelAndView.setViewName("/subject/edit");
			modelAndView.addObject("subjectDto", subjectDto);
		}
		else {
		subjectService.save(subjectDto);
		modelAndView.setViewName("redirect:/subject/all");
		}
		return modelAndView;
	}
	@GetMapping("/details")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("/subject/details");
		modelAndView.addObject("subjectDto", subjectService.findById(id));
		return modelAndView;
	}
	
	@ModelAttribute(value = "subjects")
	public List<SubjectDto> subjectDtos(){
		return subjectService.getAll();
	}
	
	@ModelAttribute(value="semesters")
	public List<Semester> semesters()
	{
		return Arrays.asList(Semester.values());
	}
	
	@ModelAttribute(value = "subjectDto")
	public SubjectDto subjectDto() {
		return new SubjectDto();
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	@ExceptionHandler(DataNotFoundException.class)
	public ModelAndView dataNotFound(DataNotFoundException dataNotFoundException) {
		ModelAndView modelAndView = new ModelAndView("/subject/all");
		modelAndView.addObject("errorMessage", dataNotFoundException.getMessage());
		modelAndView.addObject("subjects", subjectDtos());
		return modelAndView;
	}

}
