package milos.mijatovic.fon.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import milos.mijatovic.fon.dto.CityDto;
import milos.mijatovic.fon.dto.ExamRegistrationDto;
import milos.mijatovic.fon.dto.ProfessorDto;
import milos.mijatovic.fon.dto.TitleDto;
import milos.mijatovic.fon.exception.DataNotFoundException;
import milos.mijatovic.fon.service.CityService;
import milos.mijatovic.fon.service.ExamRegistrationService;
import milos.mijatovic.fon.service.ExamService;
import milos.mijatovic.fon.service.ProfessorService;
import milos.mijatovic.fon.service.StudentService;
import milos.mijatovic.fon.service.TitleService;
import milos.mijatovic.fon.validator.ProfessorDtoValidator;


@Controller
@RequestMapping(value = "/professor")
public class ProfessorController {
	private final CityService cityService;
	private final TitleService titleService;
	private final ProfessorService professorService;
	private final StudentService studentService;
	private final ExamService examService;
	private final ExamRegistrationService examRegistrationService;
	
	
	@Autowired
	public ProfessorController(ProfessorService professorService, CityService cityService, TitleService titleService, StudentService studentService, ExamService examService, ExamRegistrationService examRegistrationService) {
		this.professorService = professorService;
		this.cityService = cityService;
		this.titleService = titleService;
		this.studentService = studentService;
		this.examService = examService;
		this.examRegistrationService = examRegistrationService;
				}
	
	@GetMapping
	public ModelAndView home() {
		ModelAndView modelAndView = new ModelAndView("/professor/home");
		return modelAndView;
	}
	
	@GetMapping(value = "/all")
	public ModelAndView all() {
		ModelAndView modelAndView = new ModelAndView("/professor/all");
		return modelAndView;
	}
	@GetMapping(value = "/remove")
	public ModelAndView remove(@RequestParam ("id") Long id) throws DataNotFoundException {
		ModelAndView modelAndView = new ModelAndView("redirect:/professor/all");
		ProfessorDto professorDto = professorService.findById(id);
		examService.findByProfessor(professorDto);
		professorService.deleteById(id);
		return modelAndView;
	}
	@GetMapping(value = "/add")
	public ModelAndView add(SessionStatus sessionStatus) {
		ModelAndView modelAndView = new ModelAndView("/professor/add");
		return modelAndView;
	}
	@PostMapping(value = "/save")
	public ModelAndView save(@Valid @ModelAttribute ("professorDto") ProfessorDto professorDto, Errors errors) {
		ModelAndView modelAndView = new ModelAndView();
		if(errors.hasErrors()) {
			modelAndView.setViewName("/professor/add");
			modelAndView.addObject("professorDto", professorDto);
			System.out.println(professorDto);
		}
		else {
			professorService.save(professorDto);
			modelAndView.setViewName("redirect:/professor/all");
		}
		return modelAndView;
	}
	@GetMapping(value = "/edit")
	public ModelAndView edit(@RequestParam ("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("/professor/edit");
		modelAndView.addObject("professorDto", professorService.findById(id));
		return modelAndView;
	}
	
	@PostMapping(value = "/update")
	public ModelAndView update(@Valid @ModelAttribute ("professorDto") ProfessorDto professorDto, Errors errors, SessionStatus sessionStatus) {
		ModelAndView modelAndView = new ModelAndView();
		if(errors.hasErrors()) {
			modelAndView.setViewName("/professor/edit");
			modelAndView.addObject("professorDto", professorDto);
		}
		else {
		professorService.save(professorDto);
		modelAndView.setViewName("redirect:/professor/all");
		}
		return modelAndView;
	}
	
	@GetMapping("/details")
	public ModelAndView details(@RequestParam("id") Long id) {
		ModelAndView modelAndView = new ModelAndView("/professor/details");
		ProfessorDto professorDto = professorService.findById(id);
		List<ExamRegistrationDto> examRegistrations = examRegistrationService.registeredExamsProfessor(id);
		modelAndView.addObject("professorDto", professorDto);
		modelAndView.addObject("examRegistrations", examRegistrations);
		return modelAndView;
	}
	@ModelAttribute(name = "professors")
	public List<ProfessorDto> professorDtos(){
		return professorService.getAll();
	}
	@ModelAttribute(name = "titles")
	public List<TitleDto> titleDtos(){
		return titleService.getAll();
	}
	@ModelAttribute(name = "cities")
	public List<CityDto> cityDtos(){
		return cityService.getAll();
	}
	@ModelAttribute(name = "professorDto")
	public ProfessorDto professorDto() {
		ProfessorDto professorDto= new ProfessorDto();
		return professorDto;
	}
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(new ProfessorDtoValidator(professorService, studentService));
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	@ExceptionHandler(DataNotFoundException.class)
	public ModelAndView dataNotFound(DataNotFoundException dataNotFoundException) {
		ModelAndView modelAndView = new ModelAndView("/professor/all");
		modelAndView.addObject("errorMessage", dataNotFoundException.getMessage());
		modelAndView.addObject("professors", professorDtos());
		return modelAndView;
	}

}
