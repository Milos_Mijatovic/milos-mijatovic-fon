package milos.mijatovic.fon.exception;

public class UserValidationException extends Exception {


	private static final long serialVersionUID = 1L;

	public UserValidationException() {
		// TODO Auto-generated constructor stub
	}

	public UserValidationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UserValidationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public UserValidationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UserValidationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
