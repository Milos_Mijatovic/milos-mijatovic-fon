package milos.mijatovic.fon.exception;

public class DataNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	
	private Object object;

	public DataNotFoundException() {
		// TODO Auto-generated constructor stub
	}
	
	
	public DataNotFoundException(String message) {
		super(message);
		
	}




	public DataNotFoundException(String message, Object object) {
		super(message);
		// TODO Auto-generated constructor stub
		this.object = object;
	}

	public DataNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public DataNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DataNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}







	public Object getObject() {
		return object;
	}







	public void setObject(Object object) {
		this.object = object;
	}
	
}
