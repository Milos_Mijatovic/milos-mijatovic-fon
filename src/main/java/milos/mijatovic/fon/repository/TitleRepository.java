package milos.mijatovic.fon.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import milos.mijatovic.fon.entity.TitleEntity;

@Repository
public interface TitleRepository  extends JpaRepository<TitleEntity, Long>{

}
