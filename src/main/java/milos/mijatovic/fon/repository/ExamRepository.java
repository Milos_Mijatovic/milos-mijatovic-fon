package milos.mijatovic.fon.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import milos.mijatovic.fon.entity.ExamEntity;
import milos.mijatovic.fon.entity.ProfessorEntity;
import milos.mijatovic.fon.entity.SubjectEntity;
@Repository
public interface ExamRepository extends JpaRepository<ExamEntity, Long>{
	@Query("SELECT e FROM ExamEntity e WHERE e.date > ?1 and e.subject.name = ?2")
	public List<ExamEntity> findByDateAfter(Date date, String subjectName);
	public List<ExamEntity> findByDateBetween(Date weekBefore, Date examDate);
	
	public List<ExamEntity> findByProfessor(ProfessorEntity professorEntity);
	public List<ExamEntity> findBySubject(SubjectEntity subjectEntity);
}
