package milos.mijatovic.fon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import milos.mijatovic.fon.entity.ProfessorEntity;

@Repository
public interface ProfessorRepository extends JpaRepository<ProfessorEntity, Long> {

	public List<ProfessorEntity> findByEmailIgnoreCase(String email);
}
