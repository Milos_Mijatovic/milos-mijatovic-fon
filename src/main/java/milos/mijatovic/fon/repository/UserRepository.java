package milos.mijatovic.fon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import milos.mijatovic.fon.entity.UserEntity;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long>{

	public List<UserEntity> findByUsernameAndPassword(String username, String password);
}
