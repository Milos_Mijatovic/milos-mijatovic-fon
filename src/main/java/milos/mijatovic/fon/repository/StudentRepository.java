package milos.mijatovic.fon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import milos.mijatovic.fon.entity.StudentEntity;

@Repository
public interface StudentRepository extends JpaRepository<StudentEntity, Long>{

	
	public List<StudentEntity> findByEmailIgnoreCase(String email);
	public List<StudentEntity> findByIndexNumber(String indexNumber);
}
