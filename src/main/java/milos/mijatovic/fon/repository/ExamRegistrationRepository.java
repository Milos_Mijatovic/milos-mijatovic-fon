package milos.mijatovic.fon.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import milos.mijatovic.fon.entity.ExamRegistrationEntity;
import milos.mijatovic.fon.entity.StudentEntity;

public interface ExamRegistrationRepository extends JpaRepository<ExamRegistrationEntity, Long> {
	public List<ExamRegistrationEntity> findByStudentEntity(StudentEntity studentEntity);
	@Query("SELECT e FROM ExamRegistrationEntity e WHERE e.examEntity.date > ?1 and e.examEntity.subject.id = ?2 and e.studentEntity.id =?3")
	public List<ExamRegistrationEntity> findAllExamsAfterDateByName(Date date, Long subjectId, Long studentId);
	@Query("SELECT e FROM ExamRegistrationEntity e WHERE e.studentEntity.id =?1 ORDER BY e.examEntity.date")
	public List<ExamRegistrationEntity> currentlyRegisteredExams(Long studentId);
	@Query("SELECT e FROM ExamRegistrationEntity e WHERE e.examEntity.professor.id =?1 ORDER BY e.examEntity.date")
	public List<ExamRegistrationEntity> currentlyRegisteredExamsProfessor(Long professorId);
}
